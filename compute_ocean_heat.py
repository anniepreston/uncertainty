
#                                              #
#  • compute the Ocean Heat Content (OHC)      #
#  • from CMIP climate models!                 #
#                                              #
################################################

from netCDF4 import Dataset
from datetime import datetime, timedelta
from netCDF4 import num2date, date2num

import matplotlib.pyplot as plt
import loadGData

import numpy as np
import random
import os
import csv
import time

rho = 1025 # density of seawater, kg m^-3
c = 3985 # heat capacity (?) of seawater, J kg^-1 K^-1
n_iterations = 100 # iterations for bootstrap resampling; might determine this dynamically instead
frac = 1.0 # fraction of data for sampling; 1.0 = full (i.e. 'ground truth')
path = "/Users/anniepreston/Desktop/CMIP5_GCMs/" # ugh

start_year = '2010'
start_month = '01'
end_year = '2014' # upper bound on time range (non-inclusive)
end_month = '01' # upper bound on month range (non-inclusive)

# fixme :( -- identifiers for grouping file names:
#fileIDs = ['NorESM1', 'MRI-CGCM3', 'MPI-ESM-LR', 'MIROC4h', 'IPSL-CM5A', 'inmcm4', 'CCSM4', 'MIROC-ESM-CHEM', 'GISS-E2-H', 'CNRM-CM5', 'CanESM2', 'CanCM4']
modelIDs = ['NorESM1', 'MRI-CGCM3', 'MIROC4', 'IPSL-CM5A', 'CCSM4', 'MIROC-ESM-CHEM', 'CNRM-CM5', 'CanESM2', 'CanCM4']
#modelIDs = ['IPSL-CM5A']
class Model:
    def __init__(self, path, timesteps, dimensions, cell_areas, cell_volumes, total_area, total_volume):
        self.path = path
        self.timesteps = timesteps
        self.dimensions = dimensions
        self.cell_areas = cell_areas
        self.cell_volumes = cell_volumes
        self.total_area = total_area
        self.total_volume = total_volume

class Cell:
    def __init__(self, lev, thetao):
        self.lev = lev # FIXME: rename ('levels')
        self.thetao = thetao # temps

def main():
    area_files = [];
    vol_files = [];
    theta_files = [];
    
    OHCs = [];
    volumes = [];
    
    for ID in modelIDs:
        model_dir = [dir for dir in os.listdir(path) if dir.startswith(ID)]
        area_id = 'areacello_fx_' + ID
        theta_id = 'thetao_Omon_' + ID
        vol_id = 'volcello_fx_' + ID
        """
        area_files = loadGData.get_files(area_id)
        theta_files = loadGData.get_files(theta_id)
        vol_files = loadGData.get_files(vol_id)
        """
        theta_files = [f for f in os.listdir(path + "/" + model_dir[0] + "/") if f.startswith('thetao')]
        area_files = [f for f in os.listdir(path + "/" + model_dir[0] + "/") if f.startswith('areacello')]
        vol_files = [f for f in os.listdir(path + "/" + model_dir[0] + "/") if f.startswith('volcello')]
        
        start = time.time()

        model_ohc = []
        
        #m = initModel(ID, theta_files['files'][0]['name'])
        #m = initModel(ID, path + model_dir[0] + "/" + theta_files[0])
        """
        indices = []
        if frac != 1.0:
            n_samples = int(frac * m.dimensions[0] * m.dimensions[1])
            n_data = m.dimensions[0] * m.dimensions[1]
            indices = np.random.choice(n_data, n_samples, False)
        else:
            indices = np.arange(m.dimensions[0]*m.dimensions[1])
            """
        
        #for f in area_files:
        #    readAreaFile(path + model_dir[0] + "/" + f, m, indices)
        for f in vol_files:
            print(f)
            #readVolFile(path + model_dir[0] + "/" + f, m, indices)
            volumes.append(readVolFile(path + model_dir[0] + "/" + f))
        #for f in theta_files:
        #    readFile(path + model_dir[0] + "/" + f, m, indices)
    
    #volumes.append(m.total_volume)
        #model_ohc = calculateOHC(m, frac, indices)
        #OHCs.append(model_ohc)
    """
    filename = 'OHCs_' + start_year + '-' + end_year + '_' + str(frac) + '.csv'
    with open(filename, 'a') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for row in OHCs:
            writer.writerow(row)
            """
    filename = 'volumes_' + str(frac) + '.csv'
    with open(filename, 'a') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        writer.writerow(volumes)

        end = time.time()
        print("time elapsed: ", end - start)

def initModel(model_path, filename):
    rootgrp = Dataset(filename, 'r', format='NETCDF4')
    lat = rootgrp.variables["lat"]
    lon = rootgrp.variables["lon"]
    if (lat.shape == lon.shape): dims = lat.shape
    else: dims = (lat.shape[0], lon.shape[0])
    timestamps = np.arange(str(start_year) + '-01', str(end_year) + '-01', dtype='datetime64[M]') # NOTE: interval doesn't include stop value!
    timesteps = {}
    for t in timestamps: timesteps[str(t)] = {}
    return Model(model_path, timesteps, dims, {}, {}, 0.0, 0.0)

def readFile(filename, model, indices): # primary data file, including thetao (sea water potential temp.) values
    # fill all cells with levels & thetao values
    rootgrp = Dataset(filename, 'r', format='NETCDF4')

    thetao = rootgrp.variables["thetao"]
    times = rootgrp.variables["time"]
    dates = num2date(times[:], units=times.units,calendar=times.calendar)
    level = rootgrp.variables["lev"]
    lat = rootgrp.variables["lat"]  # latitude
    lon = rootgrp.variables["lon"]  # longitude

    # figure out which timesteps we're modifying
    for d in range(len(dates)):
        if dates[d].year >= int(start_year) and dates[d].year < int(end_year):
            for idx in indices:
                i = int(idx % model.dimensions[0])
                j = int((idx - i) / model.dimensions[0])
                theta_per_level = thetao[d,:,i,j]
                levels = level
                cell = Cell(levels, theta_per_level)
                timestamp = str(dates[d].year) + '-' + str(dates[d].month).zfill(2)
                coord = str(i) + ',' + str(j)
                model.timesteps[timestamp][coord] = cell

def readAreaFile(filename, model, indices): # 'associated file' with cell areas
    rootgrp = Dataset(filename, 'r', format='NETCDF4')
    area = rootgrp.variables["areacello"]
    cell_areas = {}
    total_area = 0
    total_volume = 0
    # look up the cell for this (lat, lon) and assign it the corresponding area
    for idx in indices:
        i = int(idx % model.dimensions[0])
        j = int((idx - i) / model.dimensions[0])
        coord = str(i) + ',' + str(j)
        cell_areas[coord] = area[i][j] # ??? CHECK
        if area[i][j] > 0:
            total_area += area[i][j]
    model.total_area = total_area
    model.cell_areas = cell_areas

#def readVolFile(filename, model, indices):
def readVolFile(filename):
    rootgrp = Dataset(filename, 'r', format='NETCDF4')
    
    lat = rootgrp.variables["lat"]
    lon = rootgrp.variables["lon"]
    if (lat.shape == lon.shape): dims = lat.shape
    else: dims = (lat.shape[0], lon.shape[0])
    
    indices = np.arange(dims[0]*dims[1])

    vol = rootgrp.variables["volcello"]
    cell_volumes = {}
    total_volume = 0
    for idx in indices:
        #i = int(idx % model.dimensions[0])
        i = int(idx % dims[0])
        #j = int((idx - i) / model.dimensions[0])
        j = int((idx - i) / dims[0])
        coord = str(i) + ',' + str(j)
        cell_volumes[coord] = []
        k = 0
        in_bounds = True
        while in_bounds:
            try: 
                volume = vol[k][j][i]
                cell_volumes[coord].append(volume)
                if volume > 0:
                    total_volume += volume
                k+=1
            except IndexError:
                in_bounds = False
    #model.cell_volumes = cell_volumes
    #model.total_volume = total_volume
    return total_volume

def calculateOHC(model, frac, indices):
    timestamps = np.arange(str(start_year) + '-01', str(end_year) + '-01', dtype='datetime64[M]')
    OHCs = []

    for t in timestamps:
        volume_sum = 0.0
        ohc = 0
        for idx in indices:
            i = int(idx % model.dimensions[0])
            j = int((idx - i) / model.dimensions[0])
            sum = 0 # running total of quantity (T * volume)
            coord = str(i) + ',' + str(j)
            cell = model.timesteps[str(t)][coord]
            for k in range(len(model.cell_volumes[coord])):
                volume = model.cell_volumes[coord][k]
                thetao = cell.thetao[k]
                if volume > 0 and thetao > 0:
                    sum += thetao*volume
                    volume_sum += volume
            q_cell = sum * rho * c
            if q_cell > 0:
                ohc += q_cell
        OHCs.append(ohc)
        volume_frac = volume_sum/model.total_volume
    return OHCs

def calcMMM(OHCs):
    MMMs = []
    for i in range(len(OHCs[0])):
        timestep_avg = 0
        for ohc in OHCs:
            timestep_avg += ohc[i]
        timestep_avg = timestep_avg/float(len(OHCs))
        MMMs.append(timestep_avg)
    return MMMs # multi-model means, one per timestep, for this ensemble

def calcVariance(MMMs):
    means = []
    variances = []
    for i in range(len(MMMs[0])): # calc average per timestep:
        avg = 0
        for mmm in MMMs:
            avg += mmm[i]
        avg = avg/float(len(MMMs))
        means.append(avg)
    for i in range(len(MMMs[0])): # calc variance per timestep
        sum = 0
        avg = means[i]
        for mmm in MMMs:
            sum += (mmm[i] - avg)*(mmm[i] - avg)
        sum = sum/float(len(MMMs))
        variances.append(sum)
    return variances # 1D array (per-timestep) of variances

main()
