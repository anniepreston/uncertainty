clear variables;
clc;

format long;

% Load Data
path = '/Users/annie/repos/uncertainty/interface/data/cmip/';

NBins = 48;
NSubsets = 6;

sizes = [0.0001 0.0002 0.0005 0.001 0.002 0.005]; % fractions
% Note: can have better fit with only the last 3 fractions (their curves are more similar)
 
% load output data
data = cell(0);
for i = 1 : NSubsets
    filePath = strcat(path, 'CMIP_variance_N_200_frac_', num2str(sizes(i)), '_adjusted.csv');
    disp(filePath);
    file = fopen(filePath, 'r');
    disp(file);
    new_cell = textscan(file, '%s\t%f\t%f\t%f\t%f\t%f\t%f\t');
    data = [data; new_cell];
    fclose(file);
end

% load the ground truth data
truth_data = cell(0);
filePath = strcat(path, 'CMIP_variance_N_200_frac_', '1.0_adjusted.csv');
disp(filePath);
file = fopen(filePath, 'r');
new_cell = textscan(file, '%s\t%f\t%f\t%f\t%f\t%f\t%f\t');
truth_data = [truth_data; new_cell];
fclose(file);

inputIndices = (1 : NBins)';
inputFractions = log([sizes, 1.00]' .^ (1/48));
k = input('Column Number (2-7): ');
for i = 1:NSubsets
    disp(data);
    outputData(1:NBins,i) = data{i,k}(1:NBins);
end
% fill the rest of the data with the ground truth (outputs to be masked) :
outputData(1:NBins,NSubsets+1) = truth_data{1,k}(1:NBins);

% mask the output ground truth data:
mask = ones(size(outputData));
mask(1:NBins,NSubsets+1) = 0;

% fill training input and output arrays
% N x P input vector, for N datapoints and P total input dimensions
xg = {inputIndices,inputFractions};

x = covGrid('expand',xg);
N = size(x,1);

% label the f=1.0 instances as missing data
mask = double(mask);
mask(mask==false)=NaN;
tmp = isnan(mask);
tmp = tmp(:);
idx = find(~tmp); % training input locations
idxstar = find(tmp); % missing input locations

d = outputData(:); %FIXME: output data format could be wrong
y = d(idx); % training data

% standardize data here??
my = mean(y);
sy = std(y);
y = ((y - my) ./ sy);

% specify a spectral mixture kernel in each input dimension, each with Qi components.
Q1 = 100;
Q2 = 100;
cov = {{'covSMfast',Q1},{'covSMfast',Q2}}; % 1D SM kernel for each input dimension, with Q components
covg = {@covGrid, cov, xg};

% mean function for the GP
gpmean = {@meanConst};
hyp.mean = mean(y); % if normalized, mean(y) will be zero

opt.cg_maxit = 20000; opt.vg_tol = 1e-4; % determine iterations here!

inf_method = @(varargin) infGrid(varargin{:},opt);
lik = @likGauss;

% initialize the noise standard deviation (?)
sn = .0001*mean(abs(y));
hyp.lik = log(sn);

% initialize spectral mixture kernel hyperparameters
inits = 100;
hyp = spectral_init(inf_method, hyp, gpmean, lik, cov, covg, x, y, idx, inits);

iters = 200;

% if you wish to use BFGS instead of non-linear conjugate gradients (see original code...)
%...

tic
hyp1 = minimize(hyp,@gp,-iters,inf_method,gpmean,covg,lik,idx,y);
toc

tic
[postg nlZg dnlZg] = infGrid(hyp1, gpmean, covg, 'likGauss', idx, y, opt);
toc

postg.L = @(x) 0*x;

% indices of points where we wish to make predictions:
% make predictions at all N input locations (training and testing)

star_ind = (1:N)';

tic
[ymug ys2g fmug fs2g] = gp(hyp1, @infGrid, gpmean, covg, [], idx, postg, star_ind);
toc

% adjust for normalization
ymug = ymug*sy + my;

disp("GPatt results:");
disp(ymug);
disp("ground truth:");
disp(d);

figure
subplot(5,1,1)
plot(inputIndices, outputData(1:NBins, NSubsets+1), inputIndices, ymug(N - NBins + 1 : N));
title('1.0')

subplot(5,1,2)
plot(inputIndices, outputData(1:NBins, 6), inputIndices, ymug(5 * NBins + 1 : 6 * NBins));
title('0.005') 

subplot(5,1,3)
plot(inputIndices, outputData(1:NBins, 4), inputIndices, ymug(3 * NBins + 1 : 4 * NBins));
title('0.001')

subplot(5,1,4)
plot(inputIndices, outputData(1:NBins, 3), inputIndices, ymug(2 * NBins + 1 : 3 * NBins));
title('0.0005')

subplot(5,1,5)
plot(inputIndices, outputData(1:NBins, 1), inputIndices, ymug(1 : NBins));
title('0.0001')
%{
disp('comparing to covSE extrapolation');

cov_SE = {{'covSEiso'},{'covSEiso'}}; % 1D SE kernel for each input dimension
covg_SE = {@covGrid, cov_SE, xg};

% initialize SE kernel hyperparameters

sn_SE = .1*mean(abs(y));
hypSE.lik = log(sn_SE);
ell = 20;
sf = std(y);
hypSE.cov = log([ell; sf; ell; sf]);

SE_iters = 1000;

% set mean function
gpmean_SE = {@meanConst};
hypSE.mean=mean(y);

tic
hypSE1 = minimize(hypSE, @gp, -SE_iters, inf_method, gpmean_SE, covg_SE, lik, idx, y);
toc

[postg_SE nlZgSE dnlZgSE] = infGrid(hypSE1, gpmean_SE, covg_SE, 'likGauss', idx, y, opt);

% indices of points where we wish to make predictions
% (for now: make predictions at all N input locations)
star_indx = (1:N)';

postg_SE.L = @(x) 0*x; % comment this line if you want predictive variances

[ymugSE ys2gSE fmugSE fs2gSE] = gp(hypSE1, @infGrid, gpmean_SE, covg_SE, [], idx, postg_SE, star_ind);
%}

