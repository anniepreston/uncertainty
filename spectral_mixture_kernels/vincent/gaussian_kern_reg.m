function zs=gaussian_kern_reg(xs, x, z, h)
format long
% Gaussian kernel function
kerf = @(z)exp(diag(-z*z'/2));

xs1 = zeros(length(x), 2);
xs1(:, 1) = (xs(:, 1) - x(:, 1))/h(1);
xs1(:, 2) = (xs(:, 2) - x(:, 2))/h(2);
d = kerf(xs1);

zs = d' * z / sum(d);
end

