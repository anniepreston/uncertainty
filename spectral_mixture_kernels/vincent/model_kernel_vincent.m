close all;
clear variables;
clc;

format long;

% Load Data
path = '/Users/annie/repos/hmfs/data/data_with_log_bins';

% Number and sizes of subsets (NOTE: using fixed values for now)
NSubsets = 6;
Link = '0.1';
sizes = [0.001 0.002 0.003 0.005 0.01 0.02];
starts = [130 130 130 130 130 130];
stops = [150 150 150 150 150 150];

NBins = 47; % Number of mass bins to predict: 48-50 has extremely small values

format long;

% Normalization parameters
A0 = 0.088582722172802; b0 = 21.982924908725941;
A1 = [0.102121353182560, 0.111934631295728, 0.121408441274172, 0.135137520119376, 0.149302048762320, 0.238462044510720]; 
b1 = [21.969386277716183, 21.959572999603015, 21.950099189624570, 21.936370110779368, 21.922205582136424, 21.833045586388021];

% Load the training data
data = cell(0);
for i = 1:NSubsets
    filePath = strcat(path, '/Variance_Average_', num2str(sizes(i)), '_', Link, '_', num2str(starts(i)), '-', num2str(stops(i)), '.dat');
    disp(filePath);
    file = fopen(filePath, 'r');
    new_cell = textscan(file, '%f\t%f\t%f\t%f\t%f\t%f\t%f\t');
    data = [data; new_cell];
    fclose(file);
end

% load the ground truth data
truth_data = cell(0);
filePath = strcat(path, '/Variance_Average_1.0_', Link, '_', num2str(30), '-', num2str(48), '.dat');
file = fopen(filePath, 'r');
new_cell = textscan(file, '%f\t%f\t%f\t%f\t%f\t%f\t%f\t');
truth_data = [truth_data; new_cell];
fclose(file);

% create arrays of training data...
N = NBins * (NSubsets + 1); %number of features

inputMasses = zeros(NBins, 1);
inputFractions = zeros(NSubsets + 1, 1);
outputData = zeros(NBins, NSubsets + 1); % output data indexed by (mass, frac)
outputData1 = zeros(NBins, NSubsets + 1); % normalized training output data
% mass:
inputMasses(:) = (1 : NBins); % logspace(1, 50, NBins);  % NOTE: here is where we define the mass bins. change as needed...
% fractions (normalized): 
for i = 1 : NSubsets
    inputFractions(i) = log(sizes(i) ^ (1/16));
end
inputFractions(7) = 0;

% reorganize and normalize data
k = input('data (2-7): ');
for i = 1 : NSubsets
    outputData(1 : NBins, i) = data{i, k}(1 : NBins);
    if i == 1
        outputData1(1 : NBins, i) = log(outputData(1 : NBins, i));
    else
        outputData1(1 : NBins, i) = (A0/A1(i - 1))*(log(outputData(1 : NBins, i)) - b1(i - 1)) + b0;
    end
end

% fill the rest of the data with the ground truth (outputs to be masked) :
outputData(1 : NBins, NSubsets + 1) = truth_data{1, k}(1 : NBins);
outputData1(1 : NBins, NSubsets + 1) = (A0/A1(6))*(log(outputData(1 : NBins, NSubsets)) - b1(6)) + b0;

% kernel regression
xtrain = zeros(N, 2);
ztrain = outputData1(:);
for i = 1 : NSubsets
    xtrain((i - 1) * NBins + 1 : i * NBins, 1) = (1 : NBins);
    xtrain((i - 1) * NBins + 1 : i * NBins, 2) = log(sizes(i) ^ (1/16));
end
xtrain(6 * NBins + 1:7 * NBins, 1) = (1:NBins);
xtrain(6 * NBins + 1:7 * NBins, 2) = 0;

h0 = [20, 20]; % initial kernel bandwidth

h = Opt_Hyp_Gauss_Ker_Reg(h0, xtrain, ztrain);% kernel bandwidth

%step lengths
Node = 50;
steplen1 = (NBins - 1) / Node;
steplen2 = (max(xtrain(:, 2)) - min(xtrain(:, 2))) / Node;

% making predictions on the grid
xs = zeros(Node + 1, 2);
x01 = min(xtrain(:, 1));
x02 = min(xtrain(:, 2));
for i = 1:Node + 1
    xs(i, 1) = x01 + steplen1 * (i - 1);
    xs(i, 2) = x02 + steplen2 * (i - 1);
end
xr = zeros(1, 2);
zr = zeros(Node + 1, Node + 1);
for i = 1:Node + 1
    xr(:, 1) = xs(i, 1);
    for j = 1:Node + 1
        xr(:, 2) = xs(j, 2);
        zr(i, j) = gaussian_kern_reg(xr, xtrain, ztrain, h);
    end
end

% get regression values for (1:47, 0)
zt = zeros(NBins, 1);
for i = 1 : NBins
    zt(i, :) = gaussian_kern_reg(xtrain(NSubsets * NBins + i, :), xtrain, ztrain, h);
end

%outputData1(1:NBins,NSubsets+1) = 13.950158266354286 + 33.142030669920715 * 0 - 0.100292135515216 * (1:NBins);
%(A0/A1(6))*(log(truth_data{1,5}(1:NBins)) - b1(6)) + b0;
% mask the output ground truth data:

% here used regression values for the ground truth, so no mask for now
outputData1(1:NBins, NSubsets + 1) = zt;
mask = ones(size(outputData));
%mask(1:NBins,NSubsets+1) = 0;

% fill training input and output arrays
% N x P input vector, for N datapoints and P total input dimensions
xg = {inputMasses,inputFractions};

x = covGrid('expand',xg); % expand the cell array xg into a multivariate (2D) grid, i.e. data points
N = size(x,1);

% label the f=1.0 instances as missing data
mask = double(mask);
mask(mask==false)=NaN;
tmp = isnan(mask);
tmp = tmp(:);
idx = find(~tmp); % training input locations
idxstar = find(tmp); % missing input locations

d = outputData1(:); %FIXME: output data format could be wrong
y = d(idx); % training data
% standardize data here??
my = mean(y);
sy = std(y);
y = ((y - my) ./ sy);
% specify a spectral mixture kernel in each input dimension, each with Q components.
Q1 = 100;
Q2 = 200;
cov = {{'covSMfast',Q1},{'covSMfast',Q2}}; % 1D SM kernel for each input dimension, with Q components
covg = {@covGrid, cov, xg};

% mean function for the GP
gpmean = {@meanConst};
hyp.mean=mean(y); % if normalized, mean(y) will be zero, BUT it isn'y

opt.cg_maxit = 20000; opt.vg_tol = 1e-5; % determine iterations here!

inf_method = @(varargin) infGrid(varargin{:},opt);
lik = @likGauss;

% initialize the noise standard deviation (?)
sn = .0001*mean(abs(y));
hyp.lik = log(sn); 

% initialize spectral mixture kernel hyperparameters
inits = 1000;
hyp = spectral_init(inf_method, hyp, gpmean, lik, cov, covg, x, y, idx, inits);

iters = 200 ;

% if you wish to use BFGS instead of non-linear conjugate gradients (see original code...)
%...

tic
hyp1 = minimize(hyp,@gp,-iters,inf_method,gpmean,covg,lik,idx,y);
toc

tic
[postg nlZg dnlZg] = infGrid(hyp1, gpmean, covg, 'likGauss', idx, y, opt);
toc

postg.L = @(x) 0*x;

% indices of points where we wish to make predictions:
% make predictions at all N input locations (training and testing)

star_ind = (1:N)';

tic
[ymug ys2g fmug fs2g] = gp(hyp1, @infGrid, gpmean, covg, [], idx, postg, star_ind);
toc

% adjust for normalization
ymug = ymug * sy + my;

disp("GPatt results:");
disp(ymug);
disp("ground truth:");
disp(d);

figure
plot3(xtrain(:, 1), xtrain(:, 2), ztrain, '.', 'MarkerSize', 15); 
hold on;
surf(xs(:, 1), xs(:, 2), zr');

figure
subplot(4,1,1)
plot(inputMasses, (A0/A1(6))*(log(truth_data{1,k}(1:NBins)) - b1(6)) + b0, inputMasses, ymug(N-NBins+1:N));
title('1.0')

subplot(4,1,2)
plot(inputMasses, outputData1(1:NBins, 5), inputMasses, ymug(4*NBins+1:5*NBins));
title('0.01') % idx = 5

subplot(4,1,3)
plot(inputMasses, outputData1(1:NBins, 4), inputMasses, ymug(3*NBins+1:4*NBins));
title('0.005') % idx = 4

subplot(4,1,4)
plot(inputMasses, outputData1(1:NBins,1), inputMasses, ymug(1:NBins));
title('0.001') % idx = 1

% visualize something here:
%{
disp('comparing to covSE extrapolation');

cov_SE = {{'covSEiso'},{'covSEiso'}}; % 1D SE kernel for each input dimension
covg_SE = {@covGrid, cov_SE, xg};

% initialize SE kernel hyperparameters

sn_SE = .1*mean(abs(y));
hypSE.lik = log(sn_SE);
ell = 20;
sf = std(y);
hypSE.cov = log([ell; sf; ell; sf]);

SE_iters = 1000;

% set mean function
gpmean_SE = {@meanConst};
hypSE.mean=mean(y);

tic
hypSE1 = minimize(hypSE, @gp, -SE_iters, inf_method, gpmean_SE, covg_SE, lik, idx, y);
toc

[postg_SE nlZgSE dnlZgSE] = infGrid(hypSE1, gpmean_SE, covg_SE, 'likGauss', idx, y, opt);

% indices of points where we wish to make predictions
% (for now: make predictions at all N input locations)
star_indx = (1:N)';

postg_SE.L = @(x) 0*x; % comment this line if you want predictive variances

[ymugSE ys2gSE fmugSE fs2gSE] = gp(hypSE1, @infGrid, gpmean_SE, covg_SE, [], idx, postg_SE, star_ind);
%}
