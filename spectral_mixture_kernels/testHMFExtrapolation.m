close all;
clear variables;
clc;

%read data

path = '/Users/anniepreston/repos/hmfs/data/';
trainSizes = [0.001 0.002 0.005 0.006 0.01 0.02];
testSizes = 0.003;
NBins = 50; % fixme

% load training data
data = cell(0);
for i = 1:length(trainSizes)
    filePath = strcat(path, num2str(sizes(i)), '_', Link, '/Variance_Average_', num2str(sizes(i)), '_', Link, '_', num2str(starts(i)), '-', num2str(stops(i)), '.dat');
    file = fopen(filePath, 'r');
    new_cell = textscan(file, '%f\t%f\t%f\t%f\t%f\t%f\t%f\t');
    data = [data; new_cell];
    fclose(file);
end

% load testing data
test_data = cell(0);
filePath = strcat(path, num2str(sizes(i+1)), '_', Link, '/Variance_Average_', num2str(sizes(i+1)), '_', Link, '_', num2str(starts(i+1)), '-', num2str(stops(i+1)), '.dat');
file = fopen(filePath, 'r');
new_cell = textscan(file, '%f\t%f\t%f\t%f\t%f\t%f\t%f\t');
test_data = [test_data; new_cell];
fclose(file);

% load ground truth
gt_data = cell(0);
filePath = strcat(path, '1.0', '_', Link, '/Variance_Average_1.0_', Link, '_', num2str(30), '-', num2str(48), '.dat'); % FIXME: don't hard-code this?
file = fopen(filePath, 'r');
new_cell = textscan(file, '%f\t%f\t%f\t%f\t%f\t%f\t%f\t');
gt_data = [gt_data; new_cell];
fclose(file);

in_dim = 8;
out_dim = 5;

NPairs = 0;
for i = 1:length(trainSizes)-1
    NPairs = NPairs + length(trainSizes) - i;
end
y = zeros(NBins * NPairs, in_dim); %training data
z = zeros(NBins * NPairs, out_dim);
idx = 1;
for i = 1:length(trainSizes)-1
    for j = i+1:length(trainSizes)
        start_idx = (idx-1)*NBins + 1;
        stop_idx = idx * NBins;
        y(start_idx:stop_idx,1) = logspace(1,10,50); % Trying a new thing: logspace sampling for mass
        y(start_idx:stop_idx,2) = sizes(i);
        y(start_idx:stop_idx,3) = sizes(j);

        y(start_idx:stop_idx, 4) = data{i,3}(1:50);
        y(start_idx:stop_idx, 5) = data{i,4}(1:50);
        y(start_idx:stop_idx, 6) = data{i,5}(1:50);
        y(start_idx:stop_idx, 7) = data{i,6}(1:50);
        y(start_idx:stop_idx, 8) = data{i,7}(1:50);
        
        z(start_idx:stop_idx, 1) = data{j,3}(1:50);
        z(start_idx:stop_idx, 2) = data{j,4}(1:50);
        z(start_idx:stop_idx, 3) = data{j,5}(1:50);
        z(start_idx:stop_idx, 4) = data{j,6}(1:50);
        z(start_idx:stop_idx, 5) = data{j,7}(1:50);

        idx = idx + 1;
    end
end

%%%

% standardize data -- omitting here

% Specify a spectral mixture kernel in each input dimension, each with 
% Q components.
Q = 20;
cov = {{'covSMfast',Q},{'covSMfast',Q},{'covSMfast',Q},{'covSMfast',Q},{'covSMfast',Q},{'covSMfast',Q},{'covSMfast',Q},{'covSMfast',Q}};  % 1D SM kernel for each input dimension, with Q components
covg = {@covGrid,cov,xg};

% specify that we want to use inf grid
% for missing data we need to run LCG.  we specify here the maximum 
% number of iterations, and the tolerance.  
% a more conservative setting would be opt.cg_maxit = 600; opt.cg_tol=1e-4
opt.cg_maxit = 200; opt.cg_tol = 1e-2; 
if(result==2 || result==3)
    opt.cg_maxit = 400;  % use more iterations for the harder problem
                         % more missing data typically requires more LCG
                         % iterations.
end

inf_method = @(varargin) infGrid(varargin{:},opt);
lik = @likGauss;

% Initialise the noise standard deviation
% 10 percent of the mean(abs(y)) is typically a good initialisation
sn = .1*mean(abs(y));  
hyp.lik = log(sn);

% initialise spectral mixture kernel hyperparameters
inits = 100; % 10 initialisations are usually sufficient for good results.  But these 
             % are cheap to evaluate, so might as well try many.
hyp = spectral_init(inf_method,hyp,gpmean,lik,cov,covg,y,y,idx,inits);


iters = 700;

% If you wish to use BFGS instead of non-linear conjugate gradients.
% BFGS typically finds better solutions more quickly, but will give up more 
% easily than the non-linear conjugate gradients implementation.
%{
% change to BGFS
p.length =    -iters;
p.method =    'BFGS';  % 'BFGS' 'LBFGS' or 'CG'
p.SIG = 0.1;
p.verbosity = 2; %0 quiet, 1 line, 2 line + warnings (default), 3 graphical
%             p.mem        % number of directions used in LBFGS (default 100)
          
tic;  
%hyp1 = minimize_new(hyp,@gp,p,inf_method,gpmean,covg,lik,idx,y);
toc;

%}

% minimize using non-linear conjugate gradients.

% final nlml for treadplate should be near 3.4e3.  If trained nlml > 4e3 
% then restart this script. Training time on a home PC should be no more than 600s.
% final nlml for treadplate with letters should be near -7.85e3.
tic
hyp1 = minimize(hyp,@gp,-iters,inf_method,gpmean,covg,lik,idx,y);
toc


tic
[postg nlZg dnlZg] = infGrid(hyp1, gpmean, covg, 'likGauss', idx, y, opt);
toc

% Here we do not need variance predictions, just predictive means.  
% So to speed things up we set L to an efficient function to evaluate.
% Comment this postg.L specification if you want the proper predictive variances.
% Note that one can only use this trick for symmetric likelihoods
postg.L = @(x) 0*x;   

% indices of points where we wish to make predictions
% make predictions at all N input locations (training and testing)
star_ind = (1:N)';

tic
[ymug ys2g fmug fs2g] = gp(hyp1, @infGrid, gpmean, covg, [], idx, postg, star_ind);
toc

% adjust for prior data normalisation
ymug = ymug*sy + my;

ypred = reshape(ymug,[100 100]);

figure(3);
imagesc(ypred);colormap(gray);
set(gca,'Xtick',[],'XTickLabel','')
set(gca,'Ytick',[],'YTickLabel','')
title('Restored Image with GPatt Extrapolation');

figure(4);
imagesc(reshape(data,[100 100]));
colormap(gray);
set(gca,'Xtick',[],'XTickLabel','')
set(gca,'Ytick',[],'YTickLabel','')
title('Original Full Image');


% Let's try with the SE kernel

disp('Comparing to covSE extrapolation')

cov_SE = {{'covSEiso'},{'covSEiso'}};  % 1D SE kernel for each input dimension
covg_SE = {@covGrid,cov_SE,xg};

% initialise SE kernel hyperparameters

sn_SE = .1*mean(abs(y));  
hypSE.lik = log(sn_SE);
ell = 20;    % initial length-scale
sf = std(y); % initial signal standard deviation
hypSE.cov = log([ell; sf; ell; sf]);

SE_iters = 1000;

% Set mean function
gpmean_SE = {@meanConst}; 
hypSE.mean=mean(y);  % if normalised, mean(y) will be zero.

tic
hypSE1 = minimize(hypSE,@gp,-SE_iters,inf_method,gpmean_SE,covg_SE,lik,idx,y);
toc


[postg_SE nlZgSE dnlZgSE] = infGrid(hypSE1, gpmean_SE, covg_SE, 'likGauss', idx, y, opt);

% indices of points where we wish to make predictions
% make predictions at all N input locations (training and testing)
star_ind = (1:N)';

postg_SE.L = @(x) 0*x;   % Comment this line if you want predictive variances

[ymugSE ys2gSE fmugSE fs2gSE] = gp(hypSE1, @infGrid, gpmean_SE, covg_SE, [], idx, postg_SE, star_ind);

% adjust for prior data normalisation
ymugSE = ymugSE*sy + my;

ypredSE = reshape(ymugSE,[100 100]);


% All together now!

figure(5)
clf;
subplot(331), imagesc(imagedata.*mask); title('Training Data')
colormap(gray);
set(gca,'Xtick',[],'XTickLabel','')
set(gca,'Ytick',[],'YTickLabel','')


reverseMask = mask;
reverseMask(reverseMask==1) = 0;
reverseMask(isnan(reverseMask)) = 1;
reverseMask(reverseMask==0) = NaN;

figure(5);
subplot(332), imagesc(imagedata.*reverseMask); title('Withheld Data');
colormap(gray);
set(gca,'Xtick',[],'XTickLabel','')
set(gca,'Ytick',[],'YTickLabel','')

subplot(333), imagesc(reshape(data,[100 100])); title('Full Pattern')
colormap(gray);
set(gca,'Xtick',[],'XTickLabel','')
set(gca,'Ytick',[],'YTickLabel','')


subplot(334), imagesc(ypred); title('GPatt (covSM) Extrapolation')
colormap(gray);
set(gca,'Xtick',[],'XTickLabel','')
set(gca,'Ytick',[],'YTickLabel','')



subplot(335), imagesc(reshape(ymugSE,[100 100])); title('covSE extrapolation')
colormap(gray);
set(gca,'Xtick',[],'XTickLabel','')
set(gca,'Ytick',[],'YTickLabel','')

% Plot the learned kernels
SM1 = cov{1};
SM2 = cov{2};

krange = [0:1:99]';

k_SM1 = feval(SM1{:},hyp1.cov(1:3*Q),0,krange);
k_SM2 = feval(SM1{:},hyp1.cov(3*Q+1:end),0,krange);

figure(5); 
subplot(337); plot(k_SM1); xlabel('\tau'); ylabel('Covariance'); title('Learned covSM Dim 1');
subplot(338); plot(k_SM2); xlabel('\tau'); ylabel('Covariance'); title('Learned covSM Dim 2');

SE1 = cov_SE{1};
SE2 = cov_SE{2};

k_SE1 = feval(SE1{:},hypSE1.cov(1:2),0,krange);
k_SE2 = feval(SE2{:},hypSE1.cov(3:4),0,krange);

figure(5);
subplot(336); plot(k_SE1); xlabel('\tau'); ylabel('Covariance'); title('Learned covSE Dim 1');
subplot(339); plot(k_SE2); xlabel('\tau'); ylabel('Covariance'); title('Learned covSE Dim 2');

