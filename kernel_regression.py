import numpy as np
np.set_printoptions(threshold=np.nan)
import csv
import math
import matplotlib.pyplot as plt

from sklearn.kernel_ridge import KernelRidge
from sklearn.model_selection import GridSearchCV
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import WhiteKernel, ExpSineSquared

import pylab as pb
import GPy

scale = 1.0e7

def main():
    path = '/Users/anniepreston/repos/hmfs/data/'
    method = input('Method: GT, TET, or MSM? ')
    output_type = input('Output: variance (v), five-number summary (f), or relative five-number summary (r)? ')
    compare_ground_truth = input('Compare result with ground truth? (y/n) ')
    test_set = compare_ground_truth == 'y'
    NSubsets = 1
    NBins = 49 # don't fix!

    if method == 'GT':
        NSubsets = 2
        sizes = np.zeros(2)
        starts = np.zeros(2)
        stops = np.zeros(2)
        sizes[0] = float(input('subset size: '))
        starts[0] = int(input('from: '))
        stops[0] = int(input('to: '))
        sizes[1] = 1.0
        starts[1] = int(input('ground truth data, from: '))
        stops[1] = int(input('ground truth data, to: '))
    else:
        NSubsets = int(input('number of sizes [2+]: '))
        sizes = np.zeros((NSubsets + test_set))
        starts = np.zeros((NSubsets + test_set))
        stops = np.zeros((NSubsets + test_set))
        for i in range(NSubsets):
            sizes[i] = float(input('subset size: '))
            starts[i] = int(input('from: '))
            stops[i] = int(input('to: '))
        if test_set == 1:
            sizes[i+1] = float(input('subset size (test): '))
            starts[i+1] = int(input('from: '))
            stops[i+1] = int(input('to: '))
    Link = input('Link: ') # FIXME: want an option for multiple linking lengths
    data = [] # FIXME

    for i in range(NSubsets):
        filePath = path + str(sizes[i]) + '_' + Link + '/Variance_Average_' + str(sizes[i]) + '_' + Link + '_' + str(math.floor(starts[i])) + '-' + str(math.floor(stops[i])) + '.dat'
        loadData(filePath, data, scale)

    data = scaleData(data)
    data = np.asarray(data)

    if test_set == 1: # load the test dataset
        test_data = []
        filePath = path + str(float(sizes[i+1])) + '_' + Link + '/Variance_Average_' + str(float(sizes[i+1])) + '_' + Link + '_' + str(int(math.floor(starts[i+1]))) + '-' + str(int(math.floor(stops[i+1]))) + '.dat'
        loadData(filePath, test_data, scale)

    test_data = scaleData(test_data)
    test_data = np.asarray(test_data)

    if compare_ground_truth == 'y': # load the 'truth' dataset
        gt_data = [] # ground truth data
        filePath = path + '1.0_' + Link + '/Variance_Average_1.0_' + Link + '_' + str(30) + '-' + str(48) + '.dat'
        loadData(filePath, gt_data, scale)

    gt_data = scaleData(gt_data)
    gt_data = np.asarray(gt_data)

    sizes = scaleSizes(sizes)

    if method == 'GT':
        if output_type == 'v':
            in_dim = 2
            out_dim = 1
        else:
            in_dim = 6
            out_dim = 5

        x = np.zeros((NBins * NSubsets, in_dim))
        x[:,0] = np.arange(NBins) #OLD METHOD -- indexing
        # new method:
        #x[:,0] = data[0,0:NBins]/data[0,NBins-1] # divide by max mass
        y = np.zeros((NBins * NSubsets, out_dim))

        if output_type == 'v':
            x[:,1] = data[1,0:NBins]
            y = data[2, 74:]
        else:
            x[:,1] = data[2,0:NBins]
            x[:,2] = data[3,0:NBins]
            x[:,3] = data[4,0:NBins]
            x[:,4] = data[5,0:NBins]
            x[:,5] = data[6,0:NBins]

            y[:,0] = data[2,NBins+1:2*NBins]
            y[:,1] = data[3,NBins+1:2*NBins]
            y[:,2] = data[4,NBins+1:2*NBins]
            y[:,3] = data[5,NBins+1:2*NBins]
            y[:,4] = data[6,NBins+1:2*NBins]

    elif method == 'TET':
        in_dim = 2
        if output_type == 'v':
            out_dim = 1
        else:
            out_dim = 5

        x = np.zeros((NBins * NSubsets, in_dim))
        for i in range(NSubsets):
            start_idx = i*NBins
            stop_idx = (i+1)*NBins - 1

            # OLD METHOD: x[start_idx:stop_idx,0] = np.arange(NBins)
            x[start_idx:stop_idx,0] = data[0,start_idx:stop_idx]/data[0,stop_idx-1] # divide by max mass

            x[start_idx:stop_idx,1] = sizes[i]

            if output_type == 'v':
                y[start_idx:stop_idx,0] = data[1,start_idx:stop_idx]
            else:
                y[start_idx:stop_idx,0] = data[2,start_idx:stop_idx]
                y[start_idx:stop_idx,1] = data[3,start_idx:stop_idx]
                y[start_idx:stop_idx,2] = data[4,start_idx:stop_idx]
                y[start_idx:stop_idx,3] = data[5,start_idx:stop_idx]
                y[start_idx:stop_idx,4] = data[6,start_idx:stop_idx]

    else: # MSM method
        if output_type == 'v':
            in_dim = 5
            out_dim = 1
        elif output_type == 'r':
            in_dim = 8
            out_dim = 4
        else:
            in_dim = 9
            out_dim = 5
        NPairs = 0
        for i in range(NSubsets-1):
            NPairs += NSubsets - (i+1)
        x = np.zeros((NBins * NPairs, in_dim))
        y = np.zeros((NBins * NPairs, out_dim))
        idx = 0
        for i in range(NSubsets-1):
            i_start = (i*NBins)
            for j in range(i+1, NSubsets):
                j_start = (j*NBins)
                start_idx = idx*NBins
                stop_idx = (idx+1)*NBins

                # x[start_idx:stop_idx,0] = np.logspace(0,1,NBins)
                # x[start_idx:stop_idx,1] = np.logspace(0,1,NBins)
                # x: mass in; mass out; frac in; frac out; v....
                
                x[start_idx:stop_idx,0] = np.arange(NBins)/NBins
                x[start_idx:stop_idx,1] = np.arange(NBins)/NBins
                #x[start_idx:stop_idx,0] = data[i_start:i_start+NBins,0]/data[i_start+NBins-1,0] # scaled mass (in)
                #x[start_idx:stop_idx,1] = data[j_start:j_start+NBins,0]/data[j_start+NBins-1,0] # scaled mass (out)
                x[start_idx:stop_idx,2] = sizes[i]
                x[start_idx:stop_idx,3] = sizes[j]
                if output_type == 'v':
                    x[start_idx:stop_idx,4] = data[i_start:i_start+NBins,4]
                    y[start_idx:stop_idx,0] = data[j_start:j_start+NBins,4]
                elif output_type == 'r':
                    i_scale = 1#np.amax(np.asarray(data[i_start:i_start+NBins,3] - data[i_start:i_start+NBins,4]))
                    j_scale = 1#np.amax(np.asarray(data[j_start:j_start+NBins,3] - data[j_start:j_start+NBins,4]))
                    x[start_idx:stop_idx,4] = (data[i_start:i_start+NBins,2] - data[i_start:i_start+NBins,4])/i_scale # min
                    x[start_idx:stop_idx,5] = (data[i_start:i_start+NBins,3] - data[i_start:i_start+NBins,4])/i_scale # max
                    x[start_idx:stop_idx,6] = (data[i_start:i_start+NBins,5] - data[i_start:i_start+NBins,4])/i_scale # lower
                    x[start_idx:stop_idx,7] = (data[i_start:i_start+NBins,6] - data[i_start:i_start+NBins,4])/i_scale # upper
                    y[start_idx:stop_idx,0] = (data[j_start:j_start+NBins,2] - data[j_start:j_start+NBins,4])/j_scale
                    y[start_idx:stop_idx,1] = (data[j_start:j_start+NBins,3] - data[j_start:j_start+NBins,4])/j_scale
                    y[start_idx:stop_idx,2] = (data[j_start:j_start+NBins,5] - data[j_start:j_start+NBins,4])/j_scale
                    y[start_idx:stop_idx,3] = (data[j_start:j_start+NBins,6] - data[j_start:j_start+NBins,4])/j_scale
                else:
                    x[start_idx:stop_idx,4] = data[i_start:i_start+NBins,2]
                    x[start_idx:stop_idx,5] = data[i_start:i_start+NBins,3]
                    x[start_idx:stop_idx,6] = data[i_start:i_start+NBins,4]
                    x[start_idx:stop_idx,7] = data[i_start:i_start+NBins,5]
                    x[start_idx:stop_idx,8] = data[i_start:i_start+NBins,6]
                    y[start_idx:stop_idx,0] = data[j_start:j_start+NBins,2]
                    y[start_idx:stop_idx,1] = data[j_start:j_start+NBins,3]
                    y[start_idx:stop_idx,2] = data[j_start:j_start+NBins,4]
                    y[start_idx:stop_idx,3] = data[j_start:j_start+NBins,5]
                    y[start_idx:stop_idx,4] = data[j_start:j_start+NBins,6]
                idx += 1
    model = fit(in_dim, x, y)
    print("training data: ", x)
    print("training truth: ", y)
    test_set, truth_set = loadTestData(test_data, gt_data, NBins, in_dim, out_dim, method, sizes)
    prediction = model.predict(test_set)[0]
    print("test data: ", test_set)
    print("truth data: ", truth_set)
    print("prediction", prediction)
    plotResults(truth_set, prediction, NBins, out_dim)

def fit(in_dim, x, y):
    """
    kr = KernelRidge()
    kr.fit(x, y)
    return kr
    """
    """
    gp_kernel = ExpSineSquared(1.0, 5.0, periodicity_bounds=(1e-2, 1e1)) \
    + WhiteKernel(1e-1)
    gpr = GaussianProcessRegressor(kernel=gp_kernel)
    gpr.fit(x, y)
    return gpr
    

    """
    k1 = GPy.kern.RatQuad(in_dim,1.,50.)
    #k2 = GPy.kern.PeriodicExponential(1, 0.5, 1.0)
    kernel = k1 #* k2
    m = GPy.models.GPRegression(x,y,kernel)
    m.optimize()
    return m

def plotResults(truth_set, prediction, NBins, out_dim):
    truth_plot_data = []
    pred_plot_data = []
    if out_dim == 1:
        x_vals = np.logspace(0,1,NBins)

        plt.scatter(x_vals, truth_set, color='g')
        plt.scatter(x_vals, prediction, color='b')
    elif out_dim == 4:
        for i in range(NBins): # FIXME: this isn't quite a real five-number summary
            truth_bin = []
            truth_bin.append(truth_set[i,0])
            truth_bin.append(truth_set[i,1])
            truth_bin.append(truth_set[i,0]*0.0)
            truth_bin.append(truth_set[i,2])
            truth_bin.append(truth_set[i,2])
            truth_bin.append(truth_set[i,3])
            truth_bin.append(truth_set[i,3])
            truth_plot_data.append(truth_bin)

            pred_bin = []
            pred_bin.append(prediction[i,0])
            pred_bin.append(prediction[i,1])
            pred_bin.append(prediction[i,0]*0.0)
            pred_bin.append(prediction[i,2])
            pred_bin.append(prediction[i,2])
            pred_bin.append(prediction[i,3])
            pred_bin.append(prediction[i,3])
            pred_plot_data.append(pred_bin)
        
        bp_truth = plt.boxplot(truth_plot_data)
        for box in bp_truth['boxes']:
            box.set(color='g')
        bp_pred = plt.boxplot(pred_plot_data)
        for box in bp_pred['boxes']:
            box.set(color='b')

    elif out_dim == 5:
        for i in range(NBins): # FIXME: this isn't quite a real five-number summary
            truth_bin = []
            truth_bin.append(truth_set[i,0])
            truth_bin.append(truth_set[i,1])
            truth_bin.append(truth_set[i,2])
            truth_bin.append(truth_set[i,3])
            truth_bin.append(truth_set[i,3])
            truth_bin.append(truth_set[i,4])
            truth_bin.append(truth_set[i,4])
            truth_plot_data.append(truth_bin)

            pred_bin = []
            pred_bin.append(prediction[i,0])
            pred_bin.append(prediction[i,1])
            pred_bin.append(prediction[i,2])
            pred_bin.append(prediction[i,3])
            pred_bin.append(prediction[i,3])
            pred_bin.append(prediction[i,4])
            pred_bin.append(prediction[i,4])
            pred_plot_data.append(pred_bin)

        bp_truth = plt.boxplot(truth_plot_data)
        for box in bp_truth['boxes']:
            box.set(color='g')
        bp_pred = plt.boxplot(pred_plot_data)
        for box in bp_pred['boxes']:
            box.set(color='b')
    plt.show()

def loadTestData(test_data, gt_data, NBins, in_dim, out_dim, method, sizes):
    test_set = np.zeros((NBins, in_dim))
    truth_set = np.zeros((NBins, out_dim))
    if method == 'GT':
        # ...
        a = 1
    elif method == 'TET':
        # ...
        a = 1
    else:
        #test_set[:,0] = np.logspace(0,1,NBins)
        #test_set[:,1] = np.logspace(0,1,NBins)
        test_set[:,0] = np.arange(NBins)/NBins
        test_set[:,1] = np.arange(NBins)/NBins
        #test_set[:,0] = test_data[0:NBins,0]/test_data[NBins-1,0] # unscaled mass (in)
        #test_set[:,1] = gt_data[0:NBins,0]/gt_data[NBins-1,0] # unscaled mass (out)
        test_set[:,2] = sizes[-1]
        test_set[:,3] = 1.0
        if in_dim == 5:
            test_set[:,4] = test_data[0:NBins,4]
            truth_set[:,0] = gt_data[0:NBins,4]
        elif in_dim == 8:
            test_scale = 1#np.amax(np.asarray(test_data[0:NBins,3] - test_data[0:NBins,4]))
            test_set[:,4] = (test_data[0:NBins,2] - test_data[0:NBins,4])/test_scale
            test_set[:,5] = (test_data[0:NBins,3] - test_data[0:NBins,4])/test_scale
            test_set[:,6] = (test_data[0:NBins,5] - test_data[0:NBins,4])/test_scale
            test_set[:,7] = (test_data[0:NBins,6] - test_data[0:NBins,4])/test_scale

            truth_scale = 1#np.amax(np.asarray(gt_data[0:NBins,3] - gt_data[0:NBins,4]))
            truth_set[:,0] = (gt_data[0:NBins,2] - gt_data[0:NBins,4])/truth_scale
            truth_set[:,1] = (gt_data[0:NBins,3] - gt_data[0:NBins,4])/truth_scale
            truth_set[:,2] = (gt_data[0:NBins,5] - gt_data[0:NBins,4])/truth_scale
            truth_set[:,3] = (gt_data[0:NBins,6] - gt_data[0:NBins,4])/truth_scale
        elif in_dim == 9:
            test_set[:,4] = test_data[0:NBins,2]
            test_set[:,5] = test_data[0:NBins,3]
            test_set[:,6] = test_data[0:NBins,4]
            test_set[:,7] = test_data[0:NBins,5]
            test_set[:,8] = test_data[0:NBins,6]

            truth_set[:,0] = gt_data[0:NBins,2]
            truth_set[:,1] = gt_data[0:NBins,3]
            truth_set[:,2] = gt_data[0:NBins,4]
            truth_set[:,3] = gt_data[0:NBins,5]
            truth_set[:,4] = gt_data[0:NBins,6]
    return test_set, truth_set

def scaleData(data):
    new_data = []
    # data: mass, variance, min, max, median, lower q, upper q
    # lots of different options here --
    for row in data:
        new_row = []
        for num in row:
            if num > 0:
                new_row.append(math.log10(num))
            else:
                new_row.append(0.0)
        new_data.append(new_row)
    return new_data

def scaleSizes(sizes):
    for s in sizes:
        s = s ** (1/3)
    return sizes

def loadData(filePath, data, scale):
    with open(filePath, 'r') as f:
        reader = csv.reader(f, delimiter='\t')
        for row in reader:
            data.append([float(num) for num in row])
    return scaleData(data)

main()