import csv
import numpy as np
from scipy.stats import scoreatpercentile
import matplotlib.pyplot as plt

# temp:
path = '/Users/annie/Desktop/fsp_speed_and_lengths/'
fn = 'lp021693_N_speeds_and_lengths.dat'

frac = 0.1
iterations = 100

pos = []
lane = []
time = []
speed = []
length = []

filename = path + fn

with open(filename, 'r') as csvfile:
    csvreader = csv.reader(csvfile, delimiter='\t')
    for row in csvreader:
        pos.append(row[0])
        lane.append(row[1])
        time.append(row[2])
        speed.append(row[3])
        length.append(row[4])

indices = [i for i, x in enumerate(speed) if x == 'In']
indices = indices[::-1]

for i in indices:
    del pos[i]
    del lane[i]
    del time[i]
    del speed[i]
    del length[i]

pos = np.asarray([float(i) for i in pos])
lane = np.asarray([float(i) for i in lane])
time = np.asarray([float(i) for i in time])
speed = np.asarray([float(i) for i in speed])
length = np.asarray([float(i) for i in length])

# based on just the one file -- TEMP
time_span_a = [1080030., 2160011.]
time_span_b = [3024028., 4320012.]

nbins = 20. # bins per time span
step_a = (time_span_a[1]-time_span_a[0])/nbins
step_b = (time_span_b[1]-time_span_b[0])/nbins

def getTimeBin(t):
    for i in range(int(nbins)):
        if t <= (time_span_a[0] + (i+1)*step_a):
            return i
    for i in range(int(nbins)):
        if t <= (time_span_b[0] + (i+1)*step_b):
            return i+int(nbins)

indices = np.random.randint(0, len(pos), int(frac*len(pos)))

pos = pos[indices]
lane = lane[indices]
time = time[indices]
speed = speed[indices]
length = length[indices]

positions = []

avg_data = {} # store all average measurements
variance_data = {} # variance data for average measurements

for t in range(int(nbins*2)):
    variance_data[t] = {}
    avg_data[t] = {}
    for p in pos:
        variance_data[t][p] = {}
        avg_data[t][p] = []
        if p not in positions: positions.append(p)

positions.sort()

# bootstrap
# and measure average speed at each position
# then get the variance of these average speeds
for i in range(iterations):
    indices = np.random.choice(len(pos), len(pos))
    data = {}
    for j in range(int(nbins*2)):
        data[j] = {}
        for p in positions:
            data[j][p] = []

    for j in range(len(indices)):
        time_bin = getTimeBin(time[indices[j]])
        p = pos[indices[j]]
        data[time_bin][p].append(speed[indices[j]])

    # now we have a dict; for each time bin, get avg speed at each marker
    for t in range(int(nbins*2)):
        for p in positions:
            if len(data[t][p]) == 0:
                avg = 0
            else:
                avg = sum(data[t][p])/float(len(data[t][p]))
            avg_data[t][p].append(avg)

# now, get variances from averages
for t in range(int(nbins*2)):
    for p in positions:
        averages = np.asarray(avg_data[t][p])
        variance_data[t][p]['med'] = np.median(averages)
        variance_data[t][p]['min'] = np.min(averages)
        variance_data[t][p]['max'] = np.max(averages)
        variance_data[t][p]['lower'] = scoreatpercentile(averages, 25)
        variance_data[t][p]['upper'] = scoreatpercentile(averages, 75)

# let's look at the data ......
for t in range(int(nbins*2)):
    x = []
    min = []
    max = []
    med = []
    lower = []
    upper = []
    for p in positions:
        d = variance_data[t][p]
        x.append(p)
        min.append(d['min'])
        max.append(d['max'])
        med.append(d['med'])
        lower.append(d['lower'])
        upper.append(d['upper'])

# write out:
    filename = './interface/data/traffic_data_40_bins/variance_t_' + str(t) + '_size_' + str(frac) + '.csv'
    with open(filename, 'w') as csvfile:
        csvwriter = csv.writer(csvfile, delimiter=',')
        csvwriter.writerow(['x', 'min', 'max', 'median', 'lower', 'upper'])
        for i in range(len(x)):
            csvwriter.writerow([x[i], min[i], max[i], med[i], lower[i], upper[i]])
