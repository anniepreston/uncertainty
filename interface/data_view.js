DataView = function(){
	var dataSource = 'hacc'; //or 'cmip'
    const colors = ["#1b9e77","#d95f02","#7570b3","#e7298a","#66a61e","#e6ab02","#a6761d","#666666"];
    var smallestFrac = 0.001; //FIXME
    var constant = 10; //FIXME

    var width = 0.11 * window.innerWidth;
	var height = 0.95 * window.innerHeight;
	
	var specWidth = 0.1*window.innerWidth;
	var xOffset = 0.005*window.innerWidth;
	
	var sampleData = [];
	var sampleToMeasure;
	var currentSample;
					  
	var randomPos = [];
	var idx = 0;
	var model_empty_sample_data = [0,1,2,3,4,5,6];
	var sample_width = (specWidth-12 - model_empty_sample_data.length - 8)/model_empty_sample_data.length;
	
	function dispatchSamples(sampleData, frac, color){
		EventBus.dispatch("addSampleEvent", this, sampleData, frac, color);
	}
	
	function dispatchOperation(frac, color){
		EventBus.dispatch("operationEvent", this, frac, color);
	}
	
	function dispatchSource(dataSource){
		EventBus.dispatch("sourceEvent", this, dataSource);
	}
	
	function addRandomPositions(frac){
	    var pos = generateRandomPositions(frac);
	    for (var i in pos){
	        pos[i][0] *= (0.5*specWidth)-24; 
	        pos[i][0] += 10 + (idx+1)%2 * 0.5 * specWidth;
	        pos[i][1] *= (0.5*specWidth)-24;
			pos[i][1] += Math.floor((idx+1)/2.0)*0.5*specWidth + 30;
	    }
	    idx++;
	    Array.prototype.push.apply(randomPos, pos);
	}
	
	var slider = new simpleSlider()
	    .width(0.3*specWidth - 6)
	    .x(17 + xOffset)
	    .y(0.5*specWidth)
	    .value(0.5)
	    .event(function(){
	        var v = slider.value();
	        var f = getFrac(v);
	        newFracText.text(f);
	        upperButtonText.style('opacity', '1.0');
	    })
	function getFrac(v){ //FIXME: this can be more sophisticated
	    var fracs = [0.001, 0.002, 0.003, 0.004, 0.005, 0.01, 0.02, 0.03, 0.04, 0.05, 0.1] //FIXME -- give more flexibility here
	    switch(true) {
	        case (v < 0.1): { return fracs[0]; break; }
	        case (v >= 0.1 && v < 0.2): { return fracs[1]; break; }
	        case (v >= 0.2 && v < 0.3): { return fracs[2]; break; }
	        case (v >= 0.3 && v < 0.4): { return fracs[3]; break; }
	        case (v >= 0.4 && v < 0.5): { return fracs[4]; break; }
	        case (v >= 0.5 && v < 0.6): { return fracs[5]; break; }
	        case (v >= 0.6 && v < 0.7): { return fracs[6]; break; }
	        case (v >= 0.7 && v < 0.8): { return fracs[7]; break; }
	        case (v >= 0.8 && v < 0.9): { return fracs[8]; break; }
	        case (v >= 0.9): { return fracs[9]; break; }
	    }
	}

	var dataContainer = d3.select("body").append("svg")
	  	.attr("width", width)
	  	.attr("height", height);
	  	
	var specGroup = dataContainer.append("g");	  
    specGroup.append("rect")
     	  .attr("x", 0)
		  .attr("y", 0)
		  .attr("height", height)
		  .attr("width", width)
		  .attr("class", "border");
		
	var baseData = specGroup.append("svg")
		.append("g")
		.attr("class", "editable");
	
	var modelData = specGroup.append("svg")
		.attr("y", 0.6*height + 80);
	
	baseData.selectAll("circle")
	    .data(generateRandomPositions(1.0))
	        .enter()
	    .append("circle")
	        .attr("cx", function(d){ return 20 + xOffset + d[0]*(specWidth-40); })
	        .attr("cy", function(d){ return 35 + d[1]*(specWidth-40); })
	        .attr("r", 1)
	        .attr("fill", "#bcbcbc");
	
	baseData.append("rect")
			.attr("x", 20 + xOffset)
			.attr("y", 35)
			.attr("width", specWidth-40)
			.attr("height", specWidth-40)
			.attr("transform", function(d, i) { return "scale(" + (1 - d / 25) * 20 + ")"; })
			.style("fill", "white")
			.attr("fill-opacity", "0.0")
			.style("stroke", "707070")
			.style("stroke-width", 3)
			.on("click", toggleSource); //FIXME -- more than toggle, for 3+ options
			
	baseData.append("text")
			.attr("class", "edit")
			.attr("x", specWidth-40)
			.attr("y", 58)
			.text(function(d) { return '\uf040'; })
			.attr("font-size", "18px")
			.attr("font-family", "FontAwesome");
			
	baseData.append("text") //"full dataset"
			.attr("x", 0.5*specWidth + xOffset)
			.attr("y", 16)
			.attr("width", 0.4*(specWidth))
			.text("Full Dataset")
			.attr("class", "header");
	var sourceText = baseData.append("text")
		.attr("x", 0.5*specWidth + xOffset)
		.attr("y", 30)
		.attr("width", 0.4*specWidth)
		.attr("class", "label")
		.text(dataSource);
			
	baseData.append("text") // FIXME: include more info
			.attr("x", 0.5*specWidth + xOffset)
			.attr("y", 0.5*specWidth + 20)
			.attr("width", 0.4*(specWidth))
			.attr("font-weight", "bold")
			.attr("text-anchor", "middle")
			.text("1.0")
			.attr("font-family", "sans-serif")
		 	.attr("font-size", "12px")
		  	.attr("fill", "#707070");
		  	
	var availData = specGroup.append("svg")
		.attr("y", specWidth + 6)
		.append('g')
		.call(slider);
	
	availData.append("text")
			.attr("x", 0.5*specWidth)
			.attr("y", 12)
			.attr("width", 0.4*(specWidth) + xOffset)
			.text("Data Samples")
			.attr("class", "header");
	
	// 'add new data sample':
	availData.append("rect")
			.attr("x", 6 + xOffset)
			.attr("y", 24)
			.attr("width", (0.5*specWidth)-12)
			.attr("height", (0.5*specWidth)-12)
			.attr("transform", function(d, i) { return "scale(" + (1 - d / 25) * 20 + ")"; })
			.style("stroke", "#a8a8a8")
			.style("fill", "none")
			.style("stroke-width", 3)
			.on("mouseover", function(){
				d3.select(this).attr("fill", "#707070").attr("fill-opacity", 0.1);
			});
			
	var upperButtonText = availData.append('text')
		.attr('x', xOffset + 0.25 * specWidth)
		.attr('y', 0.25 * specWidth + 6)
		.attr('font-weight', 'bold')
		.attr('text-anchor', 'middle')
		.text('+')
		.attr('font-family', 'sans-serif')
		.attr('font-size', '12px')
		.attr('fill', '#707070')
		.attr('opacity', '0.0')
		.on('click', addSample);		
			
	var newFracText = availData.append("text")
			.attr("x", xOffset + 0.25*specWidth)
			.attr("y", 20 + 0.25*specWidth)
			.attr("font-weight", "bold")
			.attr("text-anchor", "middle")
			.text("+")
			.attr("font-family", "sans-serif")
		 	.attr("font-size", "12px")
		  	.attr("fill", "#707070");
			
	function updateSamples() {
		availData.selectAll("svg")
			.data(sampleData)
				.enter()
			.append("rect")
				.attr("class", "sample")
				.attr("x", function(d,i){ return xOffset + 6 + (i+1)%2 * 0.5 * specWidth; })
				.attr("y", function(d,i){ return Math.floor((i+1)/2.0)*0.5*specWidth + 24; })
				.attr("width", (0.5*specWidth)-12)
				.attr("height", (0.5*specWidth)-12)
                .style("fill", "white")
                .style("fill-opacity", 0.0)
				.style("stroke", function(d,i){ return colors[i];})
				.style("stroke-width", 3)
				.on("click", handleDataSelection);
							
		availData.selectAll("frac-label")
			.data(sampleData)
				.enter()
			.append("text")
			.attr("x", function(d,i){ return xOffset + 6 + (i+1)%2 * 0.5 * specWidth + 0.5*((0.5*specWidth)-12); })
			.attr("y", function(d,i){ return Math.floor((i+1)/2.0)*0.5*specWidth + 24 + 0.25*specWidth; })
			.attr("width", 0.4*(specWidth))
			.attr("font-weight", "bold")
			.attr("text-anchor", "middle")
			.text(function(d){ return d['size']; })
			.attr("font-family", "sans-serif")
			.attr("font-size", "12px")
			.attr("fill", "#707070");	
		
		addRandomPositions(sampleData[sampleData.length-1]['size']);
		
		availData.selectAll("circle")
			.data(randomPos)
				.enter()
			.append("circle")
				.attr("cx", function(d){ return xOffset + d[0]; })
				.attr("cy", function(d){ return d[1]; })
				.attr("r", 1)
				.attr("fill", "#bcbcbc");
				
		modelData.selectAll("svg")
			.data(sampleData)
				.enter()
			.append('rect')
			.attr('x', function(d,i) { return xOffset + 14 + i * (sample_width); })
			.attr('y', 52)
			.attr("width", sample_width)
			.attr("height", sample_width)
			.style("fill", function(d,i){ return colors[i]; })
			.style("stroke", function(d,i){ return colors[i]; })
			.style("stroke-width", 1);	
	}
			
	var operationData = specGroup.append("svg")
			.attr("y", 0.5*height + 66); //FIXME!

	operationData.append("text")
		.attr("x", xOffset + 0.5*specWidth)
		.attr("y", 24)
		.attr("width", 0.4*(0.3*width))
		.attr("class", "header")
		.text("Operations");
	operationData.append("text")
		.attr("x", xOffset + 0.5*specWidth)
		.attr("y", 50)
		.attr("width", 0.4*(0.3*width))
		.attr("class", "label")
		.text("halo mass function");
	var operationButton = operationData.append("rect")
		.attr("x", 6 + xOffset)
		.attr("y", 50 + 0.05*specWidth)
		.attr("width", specWidth - 12)
		.attr("height", 32)
		.style("fill", "white")
		.style("fill-opacity", 0.5)
		.style("stroke", "#a8a8a8")
		.style("stroke-width", 3)
		.on("mouseover", function(d){
		 	d3.select(this).style("fill", "#707070").style("fill-opacity", 0.1);
		})
		.on("mouseout", function(d){
			d3.select(this).style("fill", "white");
		})
		.on("click", function(){
			d3.select(this).style("fill", "#707070");
			performOperation();
		});
    operationData.append("text")
    	.attr("x", xOffset + 0.5*specWidth)
    	.attr("y", 70 + 0.05*specWidth)
    	.text("calculate")
    	.attr("font-size", "14px")
    	.style("font-weight", "bold")
    	.attr("text-anchor", "middle")
    	.attr("font-family", "sans-serif")
    	.style("fill", "#707070");
	
	modelData.append("text")
			.attr("x", xOffset + 0.5*specWidth)
			.attr("y", 24)
			.attr("width", 0.4*(0.3*width))
			.attr("class", "header")
			.text("Models");
			
	modelData.append("text")
		.attr("x", 0.5*specWidth + xOffset)
		.attr("y", 46)
		.attr("class", "label")
		.text("data");
		
	modelData.append('text')
		.attr('x', 0.5*specWidth + xOffset)
		.attr('y', 82)
		.attr('class', 'label')
		.text('features (in)');
		
	//FIXME
	modelData.append('text')
		.attr('x', 0.2*specWidth)
		.attr('y', 98)
		.attr('fill', '#707070')
		.attr("font-size", "12px")
		.attr("font-family", "sans-serif")
		.attr("text-anchor", "left")
		.attr("font-weight", "bold")
		.text('• size');
	modelData.append('text')
		.attr('x', 0.2*specWidth)
		.attr('y', 114)
		.attr('fill', '#707070')
		.attr("font-size", "12px")
		.attr("font-family", "sans-serif")
		.attr("text-anchor", "left")
		.attr("font-weight", "bold")
		.text('• mass');
	modelData.append('text')
		.attr('x', 0.2*specWidth)
		.attr('y', 148)
		.attr('fill', '#707070')
		.attr("font-size", "12px")
		.attr("font-family", "sans-serif")
		.attr("text-anchor", "left")
		.attr("font-weight", "bold")
		.text('• variance');
		
	modelData.append('text')
		.attr('x', 0.5*specWidth + xOffset)
		.attr('y', 132)
		.attr('class', 'label')
		.text('features (out)');
	
	modelData.append("rect")
		.attr("x", 6 + xOffset)
		.attr("y", 32)
		.attr("width", specWidth - 12)
		.attr("height", 140)
		.attr("fill", "#707070")
		.attr("fill-opacity", "0.1")
		.style("stroke", "#707070")
		.style("stroke-width", 3);
		
	modelData.selectAll("svg")
		.data(model_empty_sample_data)
		.enter()
		.append('rect')
		.attr('x', function(d) { return xOffset + 14 + d * (sample_width); })
		.attr('y', 52)
		.attr("width", sample_width)
		.attr("height", sample_width)
		.style("fill", "bcbcbc")
		.style("stroke", "#a8a8a8")
		.style("stroke-width", 1);	
		
	var predictButton = modelData.append("rect")
		.attr("x", 6 + xOffset)
		.attr("y", 180)
		.attr("width", specWidth - 12)
		.attr("height", 32)
		.style("fill", "#707070")
		.style("fill-opacity", 0.8)
		.style("stroke", "#707070")
		.style("stroke-width", 3)
		.on("mouseover", function(d){
		 	d3.select(this).style("fill", "#707070").style("fill-opacity", 1.0);
		})
		.on("mouseout", function(d){
		 	d3.select(this).style("fill", "#707070").style("fill-opacity", 0.8);
		})
		.on("click", function(){
			//performOperation();
		});
    modelData.append("text")
    	.attr("x", xOffset + 0.5*specWidth)
    	.attr("y", 200)
    	.text("update prediction")
    	.attr("font-size", "14px")
    	.style("font-weight", "bold")
    	.attr("text-anchor", "middle")
    	.attr("font-family", "sans-serif")
    	.style("fill", "#dddddd");
	
	function generateRandomPositions(frac){
	    var N = Math.round(frac*constant/smallestFrac);
	    var pos = [];
	    for (var i = 0; i < N; i++){
	        pos.push([Math.random(), Math.random()]);
	    }
	    return pos;
	}
	
	function handleDataSelection(d, i){
		d3.selectAll('.sample').style("stroke-width", 3);
		d3.select(this).style("stroke-width", 6);
		prepareSampleToMeasure(d3.select(this).style("stroke"), i);
	}

	function addSample(d,i){
		var f = newFracText.text();
		var sample = { 
			'size': f,
			'link': '0.1'
		  }; //FIXME: include iterations
		sampleData.push(sample);
		updateSamples();
		dispatchSamples(sampleData, f, colors[i]);
	}  
	
	function prepareSampleToMeasure(c, i){
		currentSample = i;
	}
	function performOperation(){
		console.log("perform operation...");
		var frac = sampleData[currentSample]['size'];
		var color = colors[currentSample];
		dispatchOperation(frac, color);
	}
	
	function updateSource(source){
		this.dataSource = source;
		dataSource = source;
		dispatchSource(source);
		sourceText.text(source);
	}
	
	function toggleSource(){
		console.log("original data source: ", dataSource);
		dataSource == 'hacc' ? updateSource('cmip') : updateSource('hacc');
	}
}