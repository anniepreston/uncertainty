TrainView = function(){
    const colors = ["#1b9e77","#d95f02","#7570b3","#e7298a","#66a61e","#e6ab02","#a6761d","#666666"];

	var features = ['fraction', 'mass', 'variance']
 
	var width = 0.17*window.innerWidth;
	var height = 0.95*window.innerHeight;
	var w = width; //FIXME
	
	const container = d3.select("body").append("svg")
	  	.attr("width", width)
	  	.attr("height", height);
	  	
	this.updateFeatures = function(event, sampleData, fraction, color){
    	updateFeatures(sampleData);
    }
    this.showOperation = function(event, fraction, color){
    	addFeatures(fraction, color);
    }
    
	const baseGroup = container.append("g");	  
    baseGroup.append("rect")
     	  .attr("x", 0)
		  .attr("y", 0)
		  .attr("height", height)
		  .attr("width", width)
		  .attr("class", "border");
		  
	var featureGroup = baseGroup.append("svg")
		.append("g");
			
	featureGroup.append("text") //"full dataset"
			.attr("class", "header")
			.attr("x", 0.5*width)
			.attr("y", 16)
			.attr("width", 0.4*(width))
			.text("Features");
	
	//FIXME!		
	var text_frac_svg = container.append("svg")
		.attr("x", 0.2*width - 40)
		.attr("y", height - 88)
		.attr("width", 120)
		.attr("height", 120);
	var text_mass_svg = container.append("svg")
		.attr("x", 0.5*width - 40)
		.attr("y", height - 88)
		.attr("width", 120)
		.attr("height", 120);
	var text_var_svg = container.append("svg")
		.attr("x", 0.8*width - 40)
		.attr("y", height - 88)
		.attr("width", 120)
		.attr("height", 120);
		/*
    modelData.append("text")
    	.attr("x", xOffset + 0.5*specWidth)
    	.attr("y", 200)
    	.text("predict")
    	.attr("font-size", "14px")
    	.style("font-weight", "bold")
    	.attr("text-anchor", "middle")
    	.attr("font-family", "sans-serif")
    	.style("fill", "#dddddd");
	*/
	
	var axisGroup = featureGroup.append("g") //FIXME
		.attr("transform", function(d){ return "translate(" + 0.2*w + ",0)"; });			
	var massAxisGroup = featureGroup.append("g")
		.attr("transform", function(){ return "translate(" + 0.5*w + ",0)"; });
	var varAxisGroup = featureGroup.append("g")
		.attr("transform", function(){ return "translate(" + 0.8*w + ",0)"; });
	
	//FIXME (add lin/log option)
	var masses = [];
	var vars = [];
	
	var fractionScale = d3.scaleLinear()
		.domain([0.0005, 1.0])
		.range([0.9*height, 46]);
		
	var massScale = d3.scaleLinear()
		.domain([0.0005, 1.0])
		.range([0.9*height, 46]);
		
	var varScale = d3.scaleLinear()
		.domain([0.0005, 1.0])
		.range([0.9*height, 46]);

	var fractionAxis = d3.axisLeft(fractionScale)
		.ticks(3);

	var massAxis = d3.axisLeft(massScale)
		.ticks(0)
		.tickFormat(d3.format(".1e"));
		
	var varAxis = d3.axisLeft(varScale)
		.ticks(0)
		.tickFormat(d3.format(".1e"));
		
	var minMass = Number.MAX_SAFE_INTEGER,
		maxMass = Number.MIN_SAFE_INTEGER,
		minVar = Number.MAX_SAFE_INTEGER,
		maxVar = Number.MIN_SAFE_INTEGER;
	console.log("minmass - pre: ", minMass);
		
	function addFeatures(fraction, color){
		var t_mass = new Textbox(text_mass_svg);
		var t_var = new Textbox(text_var_svg);
		var scale_mass_button = text_mass_svg.append("rect")
			.attr("width", 58)
			.attr("height", 20)
			.attr("x", w*.2 - 38)
			.attr("y", 54)
			.style("fill", "#707070")
			.style("fill-opacity", 0.0)
			.style("stroke", "#a8a8a8")
			.style("stroke-width", 3)
			.on("mouseover", function(d){
				d3.select(this).style("fill-opacity", 0.1);
			})
			.on("mouseout", function(d){
				d3.select(this).style("fill", "#707070").style("fill-opacity", 0.0);
			})
			.on("click", function(){
				d3.select(this).style("fill", "#707070").style("fill-opacity", 0.1);
			});
		text_mass_svg.append("text")
			.attr("x", w*.2 - 8)
			.attr("y", 68)
			.text("scale")
    		.attr("font-size", "14px")
			.style("font-weight", "bold")
			.attr("text-anchor", "middle")
			.attr("font-family", "sans-serif")
			.style("fill", "#707070");
		var scale_frac_button = text_var_svg.append("rect")
			.attr("width", 58)
			.attr("height", 20)
			.attr("x", w*.2 - 38)
			.attr("y", 54)
			.style("fill", "#707070")
			.style("fill-opacity", 0.0)
			.style("stroke", "#a8a8a8")
			.style("stroke-width", 3)
			.on("mouseover", function(d){
				d3.select(this).style("fill-opacity", 0.1);
			})
			.on("mouseout", function(d){
				d3.select(this).style("fill", "#707070").style("fill-opacity", 0.0);
			})
			.on("click", function(){
				d3.select(this).style("fill", "#707070").style("fill-opacity", 0.1);
			});
		text_var_svg.append("text")
			.attr("x", w*.2 - 8)
			.attr("y", 68)
			.text("scale")
    		.attr("font-size", "14px")
			.style("font-weight", "bold")
			.attr("text-anchor", "middle")
			.attr("font-family", "sans-serif")
			.style("fill", "#707070");
	
		var fn = 'data/hacc/Variance_Average_' + fraction + '_0.1_130-150.csv';
		d3.csv(fn, function(error, data) {
			data.forEach(function(x){
				masses.push(parseFloat(x.mass));
				vars.push({
					'min': parseFloat(x.min),
					'max': parseFloat(x.max),
					'med': parseFloat(x.median),
					'upper': parseFloat(x.upper),
					'lower': parseFloat(x.lower)
				});
				if (parseFloat(x.mass) < minMass) minMass = parseFloat(x.mass);
				if (parseFloat(x.mass) > maxMass) maxMass = parseFloat(x.mass);
				if (parseFloat(x.max) > maxVar) maxVar = parseFloat(x.max);
				if (parseFloat(x.min) < minVar) minVar = parseFloat(x.min); //FIXME
			});
		console.log("min mass: ", minMass);
		console.log("max mass: ", maxMass);
		// update scales
		varScale.domain([minVar, maxVar]);
		massScale.domain([minMass, maxMass]);
		
		var massAxis = d3.axisLeft(massScale)
			.ticks(0)
			.tickFormat(d3.format(".1e"));
		
		var varAxis = d3.axisLeft(varScale)
			.ticks(0)
			.tickFormat(d3.format(".1e"));
	
		varAxis.ticks(3);
		massAxis.ticks(3);
		
		massAxisGroup.call(massAxis);
		varAxisGroup.call(varAxis);
		
		//add circles
		varAxisGroup.selectAll("circle")
			.data(vars)
		  .enter()
		  	.append("circle")
			.attr("cx", 0)
			.attr("cy", function(d){ return varScale(d.med); })
			.attr("r", 4)
			.attr("fill", color) //FIXME
			.attr("stroke", "none");
		massAxisGroup.selectAll("circle")
			.data(masses)
		  .enter()
		  	.append("circle")
			.attr("cx", 0)
			.attr("cy", function(d){ return massScale(d); })
			.attr("r", 4)
			.attr("fill", color) //FIXME
			.attr("stroke", "none");
		});
	  }
	
	function plotFeatures(sampleData){ //FIXME: won't all be sampledata..?
		var w = width;
		var t_frac = new Textbox(text_frac_svg);
		var scale_frac_button = text_frac_svg.append("rect")
			.attr("width", 58)
			.attr("height", 20)
			.attr("x", w*.2 - 38)
			.attr("y", 54)
			.style("fill", "#707070")
			.style("fill-opacity", 0.0)
			.style("stroke", "#a8a8a8")
			.style("stroke-width", 3)
			.on("mouseover", function(d){
				d3.select(this).style("fill-opacity", 0.1);
			})
			.on("mouseout", function(d){
				d3.select(this).style("fill", "#707070").style("fill-opacity", 0.0);
			})
			.on("click", function(){
				d3.select(this).style("fill", "#707070").style("fill-opacity", 0.1);
				scaleFracs(sampleData);
			});
		text_frac_svg.append("text")
			.attr("x", w*.2 - 8)
			.attr("y", 68)
			.text("scale")
    		.attr("font-size", "14px")
			.style("font-weight", "bold")
			.attr("text-anchor", "middle")
			.attr("font-family", "sans-serif")
			.style("fill", "#707070");
			
		var fracs = [];
		for (var i = 0; i < sampleData.length; i++){ fracs.push(parseFloat(sampleData[i]['size'])); }
	
		axisGroup.call(fractionAxis);
		
		axisGroup.selectAll(".circle")
			.data(fracs)
		  .enter()
		  	.append("circle")
			.attr("cx", 0)
			.attr("cy", function(d){
				console.log("d: ", d);
				console.log("y: ", fractionScale(d));
				return fractionScale(d); })
			.attr("r", 4)
			.attr("fill", function(d,i){ return colors[i]; })
			.attr("class", "masscircle")
			.attr("stroke", "none");
		axisGroup.append("circle")
			.attr("cx", 0)
			.attr("cy", function(){ return fractionScale(1.0); })
			.attr("r", 7)
			.attr("fill", "#707070");
		axisGroup.append("text")
			.attr("x", 0)
			.attr("y", 36)
			.text("size")
			.attr("class", "label");
		massAxisGroup.append("text")
			.attr("x", 0)
			.attr("y", 36)
			.text('mass')
			.attr('class', 'label');
		varAxisGroup.append("text")
			.attr("x", 0)
			.attr("y", 36)
			.text('variance')
			.attr('class', 'label');
	}
	
	function updateFeatures(sampleData){
		plotFeatures(sampleData);
	}
	
	function scaleFracs(sampleData){
		var fracs = [];
		for (var i = 0; i < sampleData.length; i++){ 
			var f = sampleData[i]['size'];
			fracs.push(parseFloat(sampleData[i]['size'])); 
		}
		fractionScale = d3.scaleLog()
			.domain([0.0005, 1.0])
			.range([0.9*height, 46]);
			
		fractionAxis = d3.axisLeft(fractionScale)
			.ticks(3);
	
		axisGroup.selectAll(".masscircle").remove();
		
		axisGroup.call(fractionAxis);
		axisGroup.selectAll(".masscircle")
			.data(fracs)
		.enter()
		  	.append("circle")
		  	.attr("class", "enter")
			.attr("cx", 0)
			.attr("cy", function(d){ return fractionScale(d); })
			.attr("r", 4)
			.attr("fill", function(d,i){ return colors[i]; })
			.attr("stroke", "none");
		circles.exit().remove();

		axisGroup.append("circle")
			.attr("cx", 0)
			.attr("cy", function(){ return fractionScale(1.0); })
			.attr("r", 7)
			.attr("fill", "#707070");
			
	}
}