PlotCompareView = function(){
	var width = 0.7*window.innerWidth;
	var height = 0.95 * window.innerHeight;
	var xAxisHeight = 28;
	var yAxisWidth = 60;
	
	var chartHeight = 0.5*height;
	var zoomHeight = 0.5*height;
	
	var currentMode = "uncertainty (samples)";
	const modeMenu = [ //FIXME...
	    {
	        title: "uncertainty (samples)",
	        action: function(d, i){
	        } 
	    },
	    {
	        title: "variance convergence (samples)",
	        action: function(d, i){
	        
	        }
	    },
	    {
	        title: "uncertainty (predicted)",
	        action: function(d, ){
	        
	        }
	    }];
	var zoomMarginTop = 20,
	    zoomMarginBot = 20;

	var x_min = Infinity,
		x_max = -Infinity,
		y_min = Infinity,
		y_max = -Infinity;
		
	var zoom_y_min = Infinity,
		zoom_y_max = -Infinity;
	
	var sizes = ['0.001', '0.005', '0.01'];
	const colors = ["#1b9e77","#d95f02","#7570b3","#e7298a","#66a61e","#e6ab02","#a6761d","#666666"];
	
	// main view ("chart"):
	d3.csv('data/hacc/Variance_Average_' + sizes[0] + '_0.1_130-150.csv', function(error0, data0) {
		d3.csv('data/hacc/Variance_Average_' + sizes[1] + '_0.1_130-150.csv', function(error1, data1) {
			d3.csv('data/hacc/Variance_Average_' + sizes[2] + '_0.1_130-150.csv', function(error2, data2) {
	  		  if (error0) throw error;
	  		  if (error1) throw error;
	  		  if (error2) throw error;

			  var data = new Array(data0.length + data1.length + data2.length);
			  var zoomData = new Array(data0.length + data1.length + data2.length);

			  pushData = function(d, i, idx, arr, zoomArr){
			  		var x_val = parseFloat(d.mass), //fixme: needs to be universal
			  		    x_bin = idx;
					variance = parseFloat(d.variance),
					min = parseFloat(d.min),
					max = parseFloat(d.max),
					med = parseFloat(d.median),
					upper = parseFloat(d.upper),
					lower = parseFloat(d.lower);
					arr[i] = [x_val, variance, min, max, med, lower, upper];
					zoomArr[i] = [x_val, variance, min - med, max - med, 0, lower - med, upper - med, x_bin];
		
					if (x_val > x_max) x_max = x_val;
					if (x_val < x_min) x_min = x_val;
		
					if (max > y_max) y_max = max;
					if (min < y_min) y_min = min;
		
					if (max - med > zoom_y_max) zoom_y_max = max - med;
					if (min - med < zoom_y_min) zoom_y_min = min - med;
			  };
			  var i = 0;
			  var idx = 0;
			  data0.forEach(function(x) {
			  	pushData(x, i, idx, data, zoomData);
			  	i += 3;
			  	idx++;
			  });
			  i = 1;
			  idx = 0;
			  data1.forEach(function(x) {
			  	pushData(x, i, idx, data, zoomData);
			  	i += 3;
			  	idx++;
			  });
			  i = 2;
			  idx = 0;
			  data2.forEach(function(x) {
			  	pushData(x, i, idx, data, zoomData);
			  	i += 3;
			  	idx++;
			  });
	  	  	  	  
	  		  var boxWidth = 0.7*(width - yAxisWidth)/data.length;
	  
	  		  var chart = d3.box(width - yAxisWidth, chartHeight - xAxisHeight, boxWidth)
				.whiskers(iqr());
		
	  		  var zoomChart = d3.box(width - yAxisWidth, zoomHeight, boxWidth)
				.whiskers(iqr());
		
	  		  chart.x_domain([x_min, x_max]);
			  chart.y_domain([y_min, y_max]);
			  chart.width([width/data.length]);
	  
			  zoomChart.x_domain([x_min, x_max]);
			  zoomChart.y_domain([zoom_y_min, zoom_y_max]);
			  zoomChart.width([width/zoomData.length]);
    
			  var xscale = d3.scaleLog()
					  .domain([x_min, x_max])
					  .range([0, width - yAxisWidth]);
					  
			  var xBinScale = d3.scaleLinear()
			          .domain([0,data.length/3]) //FIXME
			          .range([0, width - yAxisWidth]);
			            
			  var yscale = d3.scaleLinear()
					  .domain([y_min, y_max])
					  .range([chartHeight - xAxisHeight - 8, 0])
					  .nice();
			  
			  var zoomYScale = d3.scaleLinear()
					.domain([zoom_y_min, zoom_y_max])
					.nice()
					.range([zoomHeight - xAxisHeight - 8, 0])
					.nice();

			  var xAxis = d3.axisBottom()
				.scale(xscale)
				.tickSize(10)
				.ticks(2)
				.tickFormat(d3.format(".1e"));
	
	          var binXAxis = d3.axisBottom()
	            .scale(xBinScale)
	            .tickSize(10)
	            .ticks(data.length/3);
	
			  var yAxis = d3.axisLeft()
				.tickSizeOuter(0)
				.scale(yscale)
				.tickSize(10)
				.ticks(5)
				.tickFormat(d3.format(".1e"));
		
			  var zoomYAxis = d3.axisLeft()
				.tickSizeOuter(0)
				.scale(zoomYScale)
				.tickSize(10)
				.ticks(5)
				.tickFormat(d3.format(".1e"));

			  var plotContainer = d3.select("body").append("svg")
				  .attr("width", width)
				  .attr("height", height);
		  
			  var plotGroup = plotContainer.append("g");

	  	 plotGroup.append("rect")
     	  .attr("x", 0)
		  .attr("y", 0)
		  .attr("height", height)
		  .attr("width", width)
		  .attr("class", "border");

		  var zoomGroup = plotContainer.append("g")
			.attr("x", 0)
			.attr("y", chartHeight);

		  plotGroup.selectAll("g")
			.data(data)
			  .enter()
			.append("svg")
			  .attr("class", "box")
			  .attr("x", function(d) { return yAxisWidth + xscale(d[0]); })
			  .attr("y", 0)
			  .attr("width", width)
			  .attr("height", chartHeight - xAxisHeight)
			  .attr("fill", function(d,i){ return colors[i%3]; })
			.append("g")
			  .attr("transform", "translate(0,0)")
			  .call(chart);
		  
		  zoomGroup.selectAll("g")
			.data(zoomData)
				.enter()
			.append("svg")
				.attr("class", "box")
				.attr("x", function(d, i) { return yAxisWidth + xBinScale(d[7]) + (i%3)*boxWidth + 2; }) //FIXME
				.attr("y", chartHeight)
				.attr("fill", function(d,i) { return colors[i%3]; })
				.attr("width", width)
				.attr("height", zoomHeight - xAxisHeight)
			.append("g")
			  .attr("transform", "translate(0,0)")
			  .call(zoomChart);
	  
		  plotGroup.append("svg")
			  .attr("width", width)
			  .attr("height", chartHeight)
			.append("g")
			  .attr("transform", "translate(" + yAxisWidth + "," + (chartHeight - xAxisHeight) + ")")
			  .attr("width", width)
			  .call(xAxis);
		  
		  plotGroup.append("svg")  
			  .attr("width", 60)
			  .attr("height", height - xAxisHeight)
			.append("g")
			  .attr("width", 60)
			  .attr("transform", "translate(50,4)")
			  .call(yAxis);
		 
		 zoomGroup.append("svg")
			  .attr("width", width)
			  .attr("height", chartHeight)
			.append("g")
			  .attr("transform", "translate(" + yAxisWidth + "," + (chartHeight - xAxisHeight + zoomHeight) + ")")
			  .attr("width", width)
			  .call(binXAxis);
		  zoomGroup.append("svg")
			  .attr("width", 60)
			  .attr("height", zoomHeight - xAxisHeight)
			  .attr("y", chartHeight)
			.append("g")
			  .attr("width", 60)
			  .attr("transform", "translate(" + 50 + "," + 4 + ")")
			  .call(zoomYAxis);
		  /*
		  var y_linear_button = plotGroup.append("g");
	  
		  y_linear_button.append("rect")
			  .attr("y", 3)
			  .attr("x", 3)
			  .attr("height", 18)
			  .attr("width", 18)
			  .attr("rx", 3)
			  .attr("ry", 3)
			  .attr("class", "border")
			  .style("stroke", "#424242")
			  .style("fill", "none")
			  .style("stroke-width", 2);
		  
		  y_linear_button.append("text")
			  .attr("width", 16)
			  .attr("height", 16)
			  .attr("x", 6)
			  .attr("y", 16)
			  .text("lin")
			  .attr("font-family", "sans-serif")
			  .attr("font-size", "11px")
			  .attr("text-anchor", "center")
			  .attr("fill", "black");
		  
		  var y_log_button = plotGroup.append("g");
	  
		  y_log_button.append("rect")
			  .attr("y", 3)
			  .attr("x", 24)
			  .attr("rx", 3)
			  .attr("ry", 3)
			  .attr("height", 18)
			  .attr("width", 18)
			  .attr("class", "border")
			  .style("stroke", "#424242")
			  .style("fill", "none")
			  .style("stroke-width", 2);
		  
		  y_log_button.append("text")
			  .attr("width", 16)
			  .attr("height", 16)
			  .attr("x", 26)
			  .attr("y", 16)
			  .text("log")
			  .attr("font-family", "sans-serif")
			  .attr("font-size", "11px")
			  .attr("text-anchor", "center")
			  .attr("fill", "black");
		*/
		});
	
		});
	});
		function iqr() {
		  return function(d, i) {
			var q1 = d[5],
				q3 = d[6];
			return [q1, q3];
		  };
		}
	
		var plot_drag = function(){
		  d3.select('body').style("cursor", "move");
		  if (d3.event.altKey) {
			  var p = d3.svg.mouse(self.vis.node());
			  var newpoint = {};
			  newpoint.x = self.x.invert(Math.max(0, Math.min(self.size.width,  p[0])));
			  newpoint.y = self.y.invert(Math.max(0, Math.min(self.size.height, p[1])));
			  self.points.push(newpoint);
			  self.points.sort(function(a, b) {
				if (a.x < b.x) { return -1 };
				if (a.x > b.x) { return  1 };
				return 0
			  });
			selected = newpoint;
			update();
			d3.event.preventDefault();
			d3.event.stopPropagation();
		  }   
	   }
	   var update = function() {
		   var lines = d3.select("body").select("path").attr("d", this.line(this.points)); //FIXME: for when we have grid axes
		}
		
	function multiAxis(){
	    // inputs: set of domains and ranges
	    // outputs: set of (colored??) axes
	    // with custom style
	}
}