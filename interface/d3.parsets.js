// Parallel Sets by Jason Davies, http://www.jasondavies.com/
// Functionality based on http://eagereyes.org/parallel-sets
(function() {
  d3.parsets = function() {
    var event = d3.dispatch("sortDimensions", "sortCategories"),
        dimensions_ = autoDimensions,
        dimensionFormat = String,
        tooltip_ = defaultTooltip,
        categoryTooltip = defaultCategoryTooltip,
        value_,
        spacing = 100,
        width,
        height,
        tension = 1,
        tension0,
        duration = 500;

    function parsets(selection) {
      selection.each(function(data, i) {
        var g = d3.select(this),
            ordinal = d3.scale.ordinal(),
            dragging = false,
            dimensionNames = dimensions_.call(this, data, i),
            dimensions = [],
            tree = {children: {}, count: 0},
            nodes,
            total,
            ribbon;

        d3.select(window).on("mousemove.parsets." + ++parsetsId, unhighlight);

        if (tension0 == null) tension0 = tension;
        g.selectAll(".ribbon, .ribbon-mouse")
            .data(["ribbon", "ribbon-mouse"], String)
          .enter().append("g")
            .attr("class", String);
        updateDimensions();
        if (tension != tension0) {
          var t = d3.transition(g);
          if (t.tween) t.tween("ribbon", tensionTween);
          else tensionTween()(1);
        }

        function tensionTween() {
          var i = d3.interpolateNumber(tension0, tension);
          return function(t) {
            tension0 = i(t);
            ribbon.attr("d", ribbonPath);
          };
        }

        function updateDimensions() {
          // Cache existing bound dimensions to preserve sort order.
          var dimension = g.selectAll("g.dimension"),
              cache = {};
          dimension.each(function(d) { cache[d.name] = d; });
          dimensionNames.forEach(function(d) {
            if (!cache.hasOwnProperty(d)) {
              cache[d] = {name: d, categories: []};
            }
            dimensions.push(cache[d]);
          });
          dimensions.sort(compareX);
          // Populate tree with existing nodes.
          g.select(".ribbon").selectAll("path")
              .each(function(d) {
                var path = d.path.split("\0"),
                    node = tree,
                    n = path.length - 1;
                for (var i = 0; i < n; i++) {
                  var p = path[i];
                  node = node.children.hasOwnProperty(p) ? node.children[p]
                      : node.children[p] = {children: {}, count: 0};
                }
                node.children[d.name] = d;
              });
          tree = buildTree(tree, data, dimensions.map(dimensionName), value_);
          cache = dimensions.map(function(d) {
            var t = {};
            d.categories.forEach(function(c) {
              t[c.name] = c;
            });
            return t;
          });
          (function categories(d, i) {
            if (!d.children) return;
            var dim = dimensions[i],
                t = cache[i];
            for (var k in d.children) {
              if (!t.hasOwnProperty(k)) {
                dim.categories.push(t[k] = {name: k});
              }
              categories(d.children[k], i + 1);
            }
          })(tree, 0);
          ordinal.range([]).range(d3.range(dimensions[0].categories.length));
          nodes = layout(tree, dimensions, ordinal);
          total = getTotal(dimensions);
          dimensions.forEach(function(d) {
            d.count = total;
          });
          dimension = dimension.data(dimensions, dimensionName);

          var dEnter = dimension.enter().append("g")
              .attr("class", "dimension")
              .attr("transform", function(d) { return "translate(" + d.x + ", 0)"; })
              .on("mousedown.parsets", cancelEvent);
          dimension.each(function(d) {
                d.x0 = d.x;
                d.categories.forEach(function(d) { d.y0 = d.y; });
              });
          dEnter.append("rect")
              .attr("height", height)
              .attr("x", -45)
              .attr("width", 45);
          var textEnter = dEnter.append("text")
              .attr("class", "dimension")
              .attr("transform", function(d) { 
                var dx;
                d.name == dimensions[dimensions.length-1].name ? dx = -0.2*width : dx = 0;
                return "translate(" + dx + "," + 0.97*height + ")"; })
              .attr("text-anchor", function(d) {
              	if (d.name == dimensions[dimensions.length-1].name 
              	  || d.name == dimensions[0].name)
              	  return "start";
              	else return "middle";
              });
          textEnter.append("tspan")
              .attr("class", "name")
              .text(dimensionFormatName);
              /*
          dimension
              .call(d3.behavior.drag()
                .origin(identity)
                .on("dragstart", function(d) {
                  dragging = true;
                  d.x0 = d.x;
                })
                .on("drag", function(d) {
                  d.x0 = d.x = d3.event.x;
                  for (var i = 1; i < dimensions.length; i++) {
                    if (width * dimensions[i].x < width * dimensions[i - 1].x) {
                      dimensions.sort(compareX);
                      dimensionNames = dimensions.map(dimensionName);
                      ordinal.range([]).range(d3.range(dimensions[0].categories.length));
                      nodes = layout(tree = buildTree({children: {}, count: 0}, data, dimensionNames, value_), dimensions, ordinal);
                      total = getTotal(dimensions);
                      g.selectAll(".ribbon, .ribbon-mouse").selectAll("path").remove();
                      updateRibbons();
                      updateCategories(dimension);
                      dimensions.transition().duration(duration)
                        .attr("transform", translateX)
                        .tween("ribbon", ribbonTweenX);
                      event.sortDimensions();
                      break;
                    }
                  }
                  d3.select(this)
                      .attr("transform", "translate(" + d.x + ", 0)")
                      .transition();
                  ribbon.filter(function(r) { return r.source.dimension === d || r.target.dimension === d; })
                      .attr("d", ribbonPath);
                })
                .on("dragend", function(d) {
                  dragging = false;
                  unhighlight();
                  var x0 = 45,
                      dx = (width - x0 - 2) / (dimensions.length - 1);
                  dimensions.forEach(function(d, i) {
                    d.x = x0 + i * dx;
                  });
                  transition(d3.select(this))
                      .attr("transform", "translate(" + d.x + ", 0)")
                      .tween("ribbon", ribbonTweenX);
                }));
                */
          dimension.select("text").select("tspan.sort.alpha")
              .on("click.parsets", sortBy("alpha", function(a, b) { return a.name < b.name ? 1 : -1; }, dimension));
          dimension.select("text").select("tspan.sort.size")
              .on("click.parsets", sortBy("size", function(a, b) { return a.count - b.count; }, dimension));
          dimension.transition().duration(duration)
              .attr("transform", function(d) { return "translate(" + d.x + ", 0)"; })
              .tween("ribbon", ribbonTweenX);
          dimension.exit().remove();

		  var cat_dims = dimension.filter(function(dim){
		  	return dim.name.split("_")[0] == "category";})
		  var par_dims = dimension.filter(function(dim){
		    return dim.name.split("_")[0] == "param";})
          updateCategories(cat_dims);
          updateParams(par_dims);
          updateRibbons();
        }

        function sortBy(type, f, dimension) {
          return function(d) {
            var direction = this.__direction = -(this.__direction || 1);
            d3.select(this).text(direction > 0 ? type + " »" : "« " + type);
            d.categories.sort(function() { return direction * f.apply(this, arguments); });
            nodes = layout(tree, dimensions, ordinal);
            var cat_dims = dimension.filter(function(dim){
		  	return dim.name.split("_")[0] == "category";})
		  	var par_dims = dimension.filter(function(dim){
		    return dim.name.split("_")[0] == "param";})
            updateCategories(cat_dims);
            updateParams(par_dims);
            updateRibbons();
            event.sortCategories();
          };
        }

        function updateRibbons() {
          ribbon = g.select(".ribbon").selectAll("path")
              .data(nodes, function(d) { return d.path; });
          ribbon.enter().append("path")
              .each(function(d) {
                d.source.y0 = d.source.y;
                d.target.y0 = d.target.y;
              })
              .attr("class", function(d) { return "category-" + d.major; })
              .attr("d", ribbonPath);
          //ribbon.sort(function(a, b) { return b.count - a.count; });
          ribbon.exit().remove();
          var mouse = g.select(".ribbon-mouse").selectAll("path")
              .data(nodes, function(d) { return d.path; });
          mouse.enter().append("path")
              .on("mousemove.parsets", function(d) {
                ribbon.classed("active", false);
                if (dragging) return;
                highlight(d = d.node, true);
                showTooltip(tooltip_.call(this, d));
                d3.event.stopPropagation();
              });
          mouse
              .sort(function(a, b) { return b.count - a.count; })
              .attr("d", ribbonPathStatic);
          mouse.exit().remove();
        }
        function ribbonTweenX(d) {
          var r = ribbon.filter(function(r) { return r.source.dimension.name == d.name || r.target.dimension.name == d.name; }),
              i = d3.interpolateNumber(d.x0, d.x);
          return function(t) {
            d.x0 = i(t);
            r.attr("d", ribbonPath);
          };
        }
        function ribbonTweenY(d) {
          var nodes = [d],
              r = ribbon.filter(function(r) {
                var s, t;
                if (r.source.node === d) nodes.push(s = r.source);
                if (r.target.node === d) nodes.push(t = r.target);
                return s || t;
              }),
              i = nodes.map(function(d) { return d3.interpolateNumber(d.y0, d.y); }),
              n = nodes.length;
          return function(t) {
            for (var j = 0; j < n; j++) nodes[j].y0 = i[j](t);
            r.attr("d", ribbonPath);
          };
        }

        // Highlight a node and its descendants, and optionally its ancestors.
        function highlight(d, ancestors) {
          if (dragging) return;
          var highlight = [];
          (function recurse(d) {
            highlight.push(d);
            for (var k in d.children) recurse(d.children[k]);
          })(d);
          highlight.shift();
          if (ancestors) while (d) highlight.push(d), d = d.parent;
          ribbon.filter(function(d) {
            var active = highlight.indexOf(d.node) >= 0;
            if (active) this.parentNode.appendChild(this);
            return active;
          }).classed("active", true);
        }

        // Unhighlight all nodes.
        function unhighlight() {
          if (dragging) return;
          ribbon.classed("active", false);
          hideTooltip();
        }
        
        //add sliders to parameter choice indicators
        function updateParams(g) {
          var slider = g.selectAll("g.category")
            .attr("class", "slider")
        	.data(function(d){ return d.categories; }, function(d) { return d.name; });
          var yscale = d3.scale.linear()
    		.domain([0, 100]) //FIXME
    		.range([0, height-50]) //FIXME
    		.clamp(true);
          var sliderEnter = slider.enter().append("g")
              .attr("class", "param")
        	  .attr("transform", function(d) { return "translate(0, " + d.y + ")"; });
          slider.exit().remove();
          slider.append("line")
    		  .attr("class", "track")
    		  .attr("y1", yscale.range()[0])
    		  .attr("y2", yscale.range()[1])
  		    .select(function() { return this.parentNode.appendChild(this.cloneNode(true)); })
    		  .attr("class", "track-inset")
  			.select(function() { return this.parentNode.appendChild(this.cloneNode(true)); })
    		  .attr("class", "track-overlay")
    		  .call(d3.behavior.drag()
                .on("dragstart", function(d) {
                	d.y0 = d.y;
                	dragging = true;
                })
                .on("drag", function(d) {
                  adjust(d3.event.y);
                }));
		  slider.insert("g", ".track-overlay")
    		  .attr("class", "ticks")
    		  .attr("transform", "translate(" + 18 + ",0)")
            .selectAll("text")
  			.data(yscale.ticks(10))
  			.enter().append("text")
    		  .attr("y", function(d) { return yscale(d); })
    		  .attr("text-anchor", "middle")
    		  .text(function(d) { return d; });
		  var handle = slider.insert("circle", ".track-overlay")
    		.attr("class", "handle")
    		.attr("r", 9);
          function adjust(h) {
  			//handle.attr("cy", h);
  			handle.attr("transform", "translate(0," + h + ")") 
		  }
        
        }

        function updateCategories(g) {
          var category = g.selectAll("g.category")
              .data(function(d) { return d.categories; }, function(d) { return d.name; });
          var categoryEnter = category.enter().append("g")
              .attr("class", "category")
              .attr("transform", function(d) { return "translate(0, " + d.y + ")"; });
          category.exit().remove();
          /*
          category
              .on("mousemove.parsets", function(d) {
                ribbon.classed("active", false);
                if (dragging) return;
                d.nodes.forEach(function(d) { highlight(d); });
                showTooltip(categoryTooltip.call(this, d));
                d3.event.stopPropagation();
              })
              .on("mouseout.parsets", unhighlight)
              .on("mousedown.parsets", cancelEvent)
              .call(d3.behavior.drag()
                .origin(identity)
                .on("dragstart", function(d) {
                  dragging = true;
                  d.y0 = d.y;
                })
                .on("drag", function(d) {
                  d.y = d3.event.y;
                  var categories = d.dimension.categories;
                  for (var i = 0, c = categories[0]; ++i < categories.length;) {
                    if (c.y + c.dy / 2 > (c = categories[i]).y + c.dy / 2) {
                      categories.sort(function(a, b) { return a.y + a.dy / 2 - b.y - b.dy / 2; });
                      nodes = layout(tree, dimensions, ordinal);
                      updateRibbons();
                      updateCategories(g);
                      highlight(d.node);
                      event.sortCategories();
                      break;
                    }
                  }
                  var y = 0,
                      p = spacing / (categories.length - 1);
                  categories.forEach(function(e) {
                    if (d === e) e.y0 = d3.event.y;
                    e.y = y;
                    y += e.count / total * (height - spacing) + p;
                  });
                  d3.select(this)
                      .attr("transform", function(d) { return "translate(0, " + d.y0 + ")"; })
                      .transition();
                  ribbon.filter(function(r) { return r.source.node === d || r.target.node === d; })
                      .attr("d", ribbonPath);
                })
                .on("dragend", function(d) {
                  dragging = false;
                  unhighlight();
                  updateRibbons();
                  transition(d3.select(this))
                    .attr("transform", "translate(0, " + d.y + ")")
                      .tween("ribbon", ribbonTweenX);
                }));
                */
          category.transition().duration(duration)
              .attr("transform", function(d) { var pos = d.y + d.dy/2; return "translate(0, " + pos + ")"; })
              .tween("ribbon", ribbonTweenX);

          categoryEnter.append("rect")
              .attr("height", function(d) { return 20; })
              .attr("x", -10)
              .attr("width", 20);
          categoryEnter.append("line")
              .style("stroke-width", 20);
          categoryEnter.append("text")
              .attr("dx", function(d) {
                var dx; 
              	d.dimension == dimensions[dimensions.length-1] ? dx = "-5em" : dx = "0.3em";
              	return dx;})
              .attr("dy", "1em");
          category.select("rect")
              .attr("height", function(d) { return 20; })
              .attr("class", function(d) {
                return "category-" + (d.dimension === dimensions[0] ? ordinal(d.name) : "background");
              });
          category.select("line")
              	.attr("y2", function(d) { return 20; });
          category.select("text")
              .text(truncateText(function(d) { return d.name; }, function(d) { return d.dy; }))
              .attr("x", 10);
        }
      });
    }

    parsets.dimensionFormat = function(_) {
      if (!arguments.length) return dimensionFormat;
      dimensionFormat = _;
      return parsets;
    };

    parsets.dimensions = function(_) {
      if (!arguments.length) return dimensions_;
      dimensions_ = d3.functor(_);
      return parsets;
    };

    parsets.value = function(_) {
      if (!arguments.length) return value_;
      value_ = d3.functor(_);
      return parsets;
    };

    parsets.width = function(_) {
      if (!arguments.length) return width;
      width = +_;
      return parsets;
    };

    parsets.height = function(_) {
      if (!arguments.length) return height;
      height = +_;
      return parsets;
    };

    parsets.spacing = function(_) {
      if (!arguments.length) return spacing;
      spacing = +_;
      return parsets;
    };

    parsets.tension = function(_) {
      if (!arguments.length) return tension;
      tension = +_;
      return parsets;
    };

    parsets.duration = function(_) {
      if (!arguments.length) return duration;
      duration = +_;
      return parsets;
    };

    parsets.tooltip = function(_) {
      if (!arguments.length) return tooltip;
      tooltip_ = _ == null ? defaultTooltip : _;
      return parsets;
    };

    parsets.categoryTooltip = function(_) {
      if (!arguments.length) return categoryTooltip;
      categoryTooltip = _ == null ? defaultCategoryTooltip : _;
      return parsets;
    };

    var body = d3.select("body");
    var tooltip = body.append("div")
        .style("display", "none")
        .attr("class", "parsets tooltip");

    return d3.rebind(parsets, event, "on").value(1).width(960).height(650);

    function dimensionFormatName(d, i) {
      var name = d.name;
      if (d.name.split("_").length == 2) name = d.name.split("_")[1];
      return dimensionFormat.call(this, name, i);
    }

    function showTooltip(html) {
      var m = d3.mouse(body.node());
      tooltip
          .style("display", null)
          .style("left", m[0] + 30 + "px")
          .style("top", m[1] - 20 + "px")
          .html(html);
    }

    function hideTooltip() {
      tooltip.style("display", "none");
    }

    function transition(g) {
      return duration ? g.transition().duration(duration).ease(parsetsEase) : g;
    }

    function layout(tree, dimensions, ordinal) {
      var nodes = [],
          nd = dimensions.length,
          x0 = 45,
          dx = (width - x0 - 2) / (nd - 1);
      dimensions.forEach(function(d, i) {
        d.categories.forEach(function(c) {
          c.dimension = d;
          c.count = 0;
          c.nodes = [];
        });
        d.x = x0 + i * dx;
      });

      // Compute per-category counts.
      var total = (function rollup(d, i) {
        if (!d.children) return d.count;
        var dim = dimensions[i],
            total = 0;
        dim.categories.forEach(function(c) {
          var child = d.children[c.name];
          if (!child) return;
          c.nodes.push(child);
          var count = rollup(child, i + 1);
          c.count += count;
          total += count;
        });
        return total;
      })(tree, 0);

      // Stack the counts.
      dimensions.forEach(function(d) {
        d.categories = d.categories.filter(function(d) { return d.count; });
        var y = 0,
            p = spacing / (d.categories.length - 1);
        d.categories.forEach(function(c) {
          c.y = y;
          c.dy =  1 / d.categories.length * (height - 50 - spacing);
          c.in = {dy: 0};
          c.out = {dy: 0};
          y += c.dy + p;
        });
      });

      var dim = dimensions[0];
      dim.categories.forEach(function(c) {
        var k = c.name;
        if (tree.children.hasOwnProperty(k)) {
          recurse(c, {node: tree.children[k], path: k}, 1, ordinal(k));
        }
      });

      function recurse(p, d, depth, major) {
        var node = d.node,
            dimension = dimensions[depth];
        dimension.categories.forEach(function(c) {
          var k = c.name;
          if (k == "") return;
          if (!node.children.hasOwnProperty(k)) return;
          var child = node.children[k];
          child.path = d.path + "\0" + k;
          var target = child.target || {node: c, dimension: dimension};
          target.y = c.in.dy;
          target.dy = child.count / total * (height - 50 - spacing);
          c.in.dy += target.dy;
          var source = child.source || {node: p, dimension: dimensions[depth - 1]};
          source.y = p.out.dy;
          source.dy = target.dy;
          p.out.dy += source.dy;

          child.node = child;
          child.source = source;
          child.target = target;
          child.major = major;
          nodes.push(child);
          if (depth + 1 < dimensions.length) recurse(c, child, depth + 1, major);
        });
      }
      return nodes;
    }

    // Dynamic path string for transitions.
    function ribbonPath(d) {
      var s = d.source,
          t = d.target;
      return ribbonPathString(s.dimension.x0, s.node.y0 + s.y0, s.dy, t.dimension.x0, t.node.y0 + t.y0, t.dy, tension0);
    }

    // Static path string for mouse handlers.
    function ribbonPathStatic(d) {
      var s = d.source,
          t = d.target;
      return ribbonPathString(s.dimension.x, s.node.y + s.y, s.dy, t.dimension.x, t.node.y + t.y, t.dy, tension);
    }

	/*
    function ribbonPathString(sx, sy, sdx, tx, ty, tdx, tension) {
      var m0, m1;
      return (tension === 1 ? [
          "M", [sx, sy],
          "L", [tx, ty],
          "h", tdx,
          "L", [sx + sdx, sy],
          "Z"]
       : ["M", [sx, sy],
          "C", [sx, m0 = tension * sy + (1 - tension) * ty], " ",
               [tx, m1 = tension * ty + (1 - tension) * sy], " ", [tx, ty],
          "h", tdx,
          "C", [tx + tdx, m1], " ", [sx + sdx, m0], " ", [sx + sdx, sy],
          "Z"]).join("");
    }
    */
    
    function ribbonPathString(sx, sy, sdy, tx, ty, tdy, tension) {
      var m0, m1;
      return (tension === 1 ? [
          "M", [sx, sy],
          "L", [tx, ty],
          "v", tdy,
          "L", [sx, sy + sdy],
          "Z"]
       : ["M", [sx, sy],
          "C", [sx, m0 = tension * sy + (1 - tension) * ty], " ",
               [tx, m1 = tension * ty + (1 - tension) * sy], " ", [tx, ty],
          "v", tdy,
          "C", [tx, m1], " ", [sx, m0], " ", [sx, sy + sdy],
          "Z"]).join("");
    }

    function compareY(a, b) {
      a = (height - 50) * a.y, b = (height - 50) * b.y;
      return a < b ? -1 : a > b ? 1 : a >= b ? 0 : a <= a ? -1 : b <= b ? 1 : NaN;
    }
    function compareX(a, b) {
      a = width * a.x, b = width * b.x;
      return a < b ? -1 : a > b ? 1 : a >= b ? 0 : a <= a ? -1 : b <= b ? 1 : NaN;
    }
  };
  d3.parsets.tree = buildTree;

  function autoDimensions(d) {
    return d.length ? d3.keys(d[0]).sort() : [];
  }

  function cancelEvent() {
    d3.event.stopPropagation();
    d3.event.preventDefault();
  }

  function dimensionName(d) { return d.name; }

  function getTotal(dimensions) {
    return dimensions[0].categories.reduce(function(a, d) {
      return a + d.count;
    }, 0);
  }

  // Given a text function and width function, truncates the text if necessary to
  // fit within the given width.
  function truncateText(text, width) {
    return function(d, i) {
      var t = this.textContent = text(d, i),
          w = width(d, i);
      if (this.getComputedTextLength() < w) return t;
      this.textContent = "…" + t;
      var lo = 0,
          hi = t.length + 1,
          x;
      while (lo < hi) {
        var mid = lo + hi >> 1;
        if ((x = this.getSubStringLength(0, mid)) < w) lo = mid + 1;
        else hi = mid;
      }
      return lo > 1 ? t.substr(0, lo - 2) + "…" : "";
    };
  }

  var percent = d3.format("%"),
      comma = d3.format(",f"),
      parsetsEase = "elastic",
      parsetsId = 0;

  // Construct tree of all category counts for a given ordered list of
  // dimensions.  Similar to d3.nest, except we also set the parent.
  function buildTree(root, data, dimensions, value) {
    zeroCounts(root);
    var n = data.length,
        nd = dimensions.length;
    for (var i = 0; i < n; i++) {
      var d = data[i],
          v = +value(d, i),
          node = root;
      for (var j = 0; j < nd; j++) {
        var dimension = dimensions[j];
        var category;
        var children = node.children;
        if (dimension.split("_")[0] == "category") {
            category = d[dimension];
        	}
        else if (dimension.split("_")[0] == "param") {
        	var str = d[dimension];
        	category = str.split(", ")[0] + str.split(", ")[1];
        	}
        node.count += v;
        node = children.hasOwnProperty(category) ? children[category]
            : children[category] = {
              children: j === nd - 1 ? null : {},
              count: 0,
              parent: node,
              dimension: dimension,
              name: category
            };
      }
      node.count += v;
    }
    return root;
  }

  function zeroCounts(d) {
    d.count = 0;
    if (d.children) {
      for (var k in d.children) zeroCounts(d.children[k]);
    }
  }

  function identity(d) { return d; }

  function translateY(d) { return "translate(0," + d.y + ")"; }
  function translateX(d) { return "translate(" + d.x + ", 0)"; }

  function defaultTooltip(d) {
    var count = d.count,
        path = [];
    while (d.parent) {
      if (d.name) path.unshift(d.name);
      d = d.parent;
    }
    return path.join(" → ") + "<br>" + comma(count) + " (" + percent(count / d.count) + ")";
  }

  function defaultCategoryTooltip(d) {
    return d.name + "<br>" + comma(d.count) + " (" + percent(d.count / d.dimension.count) + ")";
  }
})();
