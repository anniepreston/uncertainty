PlotView = function(){
	var width = 0.68*window.innerWidth;
	var height = 0.95 * window.innerHeight;
	var xAxisHeight = 28;
	var yAxisWidth = 60;
	
	var chartHeight = 0.7*height;
	var zoomHeight = 0.3*height;
	
	var zoomMarginTop = 20,
	    zoomMarginBot = 20;

	var x_min = Infinity,
		x_max = -Infinity,
		y_min = Infinity,
		y_max = -Infinity;
		
	var zoom_y_min = Infinity,
		zoom_y_max = -Infinity;
		
	// main view ("chart"):
	d3.csv("data/darkmatter/Variance_Average_0.02_0.1_130-150.csv", function(error, csv) {
	  if (error) throw error;

	  var data = [];
	  var zoomData = []; // normalized data view

	  csv.forEach(function(x) {
		var x_val = parseFloat(x.mass), //fixme: needs to be universal
			variance = parseFloat(x.variance),
			min = parseFloat(x.min),
			max = parseFloat(x.max),
			med = parseFloat(x.median),
			upper = parseFloat(x.upper),
			lower = parseFloat(x.lower);
		data.push([x_val, variance, min, max, med, lower, upper]);
		console.log(data);
		zoomData.push([x_val, variance, min - med, max - med, 0, lower - med, upper - med]);
		
		if (x_val > x_max) x_max = x_val;
		if (x_val < x_min) x_min = x_val;
		
		if (max > y_max) y_max = max;
		if (min < y_min) y_min = min;
		
		if (max - med > zoom_y_max) zoom_y_max = max - med;
		if (min - med < zoom_y_min) zoom_y_min = min - med;
	  });
	  
	  console.log(data);
	  
	  var boxWidth = 0.8*(width - yAxisWidth)/data.length;
	  
	  var chart = d3.box(width - yAxisWidth, chartHeight - xAxisHeight, boxWidth)
		.whiskers(iqr());
		
	  var zoomChart = d3.box(width - yAxisWidth, zoomHeight, boxWidth)
		.whiskers(iqr());
		
	  chart.x_domain([x_min, x_max]);
	  chart.y_domain([y_min, y_max]);
	  chart.width([width/data.length]);
	  
	  zoomChart.x_domain([x_min, x_max]);
	  zoomChart.y_domain([zoom_y_min, zoom_y_max]);
	  zoomChart.width([width/zoomData.length]);
    
	  var xscale = d3.scaleLog()
			  .domain([x_min, x_max])
			  .range([0, width - yAxisWidth]);
			  
	  var yscale = d3.scaleLinear()
			  .domain([y_min, y_max])
			  .range([chartHeight - xAxisHeight, 0])
			  .nice();
			  
	  var zoomYScale = d3.scaleLinear()
	  		.domain([zoom_y_min, zoom_y_max])
	  		.nice()
	  		.range([zoomHeight, 0])
	  		.nice();

	  var xAxis = d3.axisBottom()
		.scale(xscale)
		.tickSize(10)
		.ticks(6)
		.tickFormat(d3.format(".1e"));
	
	  var yAxis = d3.axisLeft()
	    .tickSizeOuter(0)
		.scale(yscale)
		.tickSize(10)
		.ticks(5)
		.tickFormat(d3.format(".1e"));
		
	  var zoomYAxis = d3.axisLeft()
	  	.tickSizeOuter(0)
	  	.scale(zoomYScale)
	  	.tickSize(10)
	  	.ticks(5)
	  	.tickFormat(d3.format(".1e"));

	  var plotContainer = d3.select("body").append("svg")
	  	  .attr("width", width)
	  	  .attr("height", height);
	  	  
	  var plotGroup = plotContainer.append("g");
	  
	  plotGroup.append("rect")
     	  .attr("x", 0)
		  .attr("y", 0)
		  .attr("height", height)
		  .attr("width", width)
		  .attr("class", "border");
	  
	  var zoomGroup = plotContainer.append("g")
	  	.attr("x", 0)
	  	.attr("y", chartHeight);

	  plotGroup.selectAll("g")
	    .data(data)
	      .enter()
        .append("svg")
		  .attr("class", "box")
		  .attr("x", function(d) { return yAxisWidth + xscale(d[0]); })
		  .attr("y", 0)
		  .attr("width", width)
		  .attr("height", chartHeight - xAxisHeight)
		.append("g")
		  .attr("transform", "translate(0,0)")
		  .call(chart);
		  
	  zoomGroup.selectAll("g")
	  	.data(zoomData)
	  		.enter()
	  	.append("svg")
	  		.attr("class", "box")
	  		.attr("x", function(d) { return yAxisWidth + xscale(d[0]); })
	  		.attr("y", chartHeight)
	  		.attr("width", width)
	  		.attr("height", zoomHeight - xAxisHeight)
	  	.append("g")
		  .attr("transform", "translate(0,0)")
		  .call(zoomChart);
	  
	  plotGroup.append("svg")
		  .attr("width", width)
		  .attr("height", chartHeight)
		.append("g")
		  .attr("transform", "translate(" + yAxisWidth + "," + (chartHeight - xAxisHeight) + ")")
		  .attr("width", width)
		  .call(xAxis);
		  
	  plotGroup.append("svg")  
		  .attr("width", 60)
		  .attr("height", height - xAxisHeight)
		.append("g")
		  .attr("width", 60)
		  .attr("transform", "translate(50,0)")
		  .call(yAxis);
		  
	  zoomGroup.append("svg")
	  	  .attr("width", 60)
	  	  .attr("height", zoomHeight)
	  	  .attr("y", chartHeight)
	  	.append("g")
	  	  .attr("width", 60)
	  	  .attr("transform", "translate(" + 50 + "," + 0 + ")")
	  	  .call(zoomYAxis);
		  
	  var y_linear_button = plotGroup.append("g");
	  
	  y_linear_button.append("rect")
	  	  .attr("y", 3)
	  	  .attr("x", 3)
		  .attr("height", 18)
		  .attr("width", 18)
		  .attr("rx", 3)
		  .attr("ry", 3)
		  .attr("class", "border")
		  .style("stroke", "#424242")
		  .style("fill", "none")
		  .style("stroke-width", 2);
		  
	  y_linear_button.append("text")
	  	  .attr("width", 16)
	  	  .attr("height", 16)
	  	  .attr("x", 6)
	  	  .attr("y", 16)
	  	  .text("lin")
	  	  .attr("font-family", "sans-serif")
		  .attr("font-size", "11px")
		  .attr("text-anchor", "center")
		  .attr("fill", "black");
	  	  
	  var y_log_button = plotGroup.append("g");
	  
	  y_log_button.append("rect")
	  	  .attr("y", 3)
	  	  .attr("x", 24)
	  	  .attr("rx", 3)
	  	  .attr("ry", 3)
		  .attr("height", 18)
		  .attr("width", 18)
		  .attr("class", "border")
		  .style("stroke", "#424242")
		  .style("fill", "none")
		  .style("stroke-width", 2);
		  
	  y_log_button.append("text")
	  	  .attr("width", 16)
	  	  .attr("height", 16)
	  	  .attr("x", 26)
	  	  .attr("y", 16)
	  	  .text("log")
	  	  .attr("font-family", "sans-serif")
		  .attr("font-size", "11px")
		  .attr("text-anchor", "center")
		  .attr("fill", "black");
	  	
	});
	
	function iqr() {
	  return function(d, i) {
		var q1 = d[5],
			q3 = d[6];
		return [q1, q3];
	  };
	}
	
	var plot_drag = function(){
	  d3.select('body').style("cursor", "move");
      if (d3.event.altKey) {
		  var p = d3.svg.mouse(self.vis.node());
		  var newpoint = {};
		  newpoint.x = self.x.invert(Math.max(0, Math.min(self.size.width,  p[0])));
		  newpoint.y = self.y.invert(Math.max(0, Math.min(self.size.height, p[1])));
		  self.points.push(newpoint);
		  self.points.sort(function(a, b) {
			if (a.x < b.x) { return -1 };
			if (a.x > b.x) { return  1 };
			return 0
      	  });
        selected = newpoint;
        update();
        d3.event.preventDefault();
        d3.event.stopPropagation();
      }   
   }
   var update = function() {
       var lines = d3.select("body").select("path").attr("d", this.line(this.points)); //FIXME: for when we have grid axes
	}

   var datapoint_drag = function() {
       return function(d) {
		   registerKeyboardHandler(self.keydown());
		   document.onselectstart = function() { return false; };
		   selected = dragged = d;
		   update(); 
  	   }
	};

	var mousemove = function() {
       return function() {
            var p = d3.mouse(plotContainer[0][0]),
            t = d3.event.changedTouches; 
		    if (dragged) {
		    	dragged.y = y.invert(Math.max(0, Math.min(self.size.height, p[1])));
		     	update();
		   	};
    		if (!isNaN(downx)) {
      			d3.select('body').style("cursor", "ew-resize");
      			var rupx = self.x.invert(p[0]),
          		xaxis1 = self.x.domain()[0],
         		xaxis2 = self.x.domain()[1],
          		xextent = xaxis2 - xaxis1;
      			if (rupx != 0) {
       				var changex, new_domain;
        			changex = downx / rupx;
        			new_domain = [xaxis1, xaxis1 + (xextent * changex)];
        			self.x.domain(new_domain);
        			self.redraw()();
      			}
      			d3.event.preventDefault();
      			d3.event.stopPropagation();
    		};
    		if (!isNaN(downy)) {
      			d3.select('body').style("cursor", "ns-resize");
      			var rupy = y.invert(p[1]),
          		yaxis1 = y.domain()[1],
          		yaxis2 = y.domain()[0],
          		yextent = yaxis2 - yaxis1;
      			if (rupy != 0) {
        			var changey, new_domain;
        			changey = self.downy / rupy;
        			new_domain = [yaxis1 + (yextent * changey), yaxis1];
        			self.y.domain(new_domain);
        			self.redraw()();
      			}
      			d3.event.preventDefault();
      			d3.event.stopPropagation();
    		}
  		}
	};

	var mouseup = function() {
  		return function() {
    		document.onselectstart = function() { return true; };
    		d3.select('body').style("cursor", "auto");
    		d3.select('body').style("cursor", "auto");
    		if (!isNaN(self.downx)) {
				redraw()();
				downx = Math.NaN;
				d3.event.preventDefault();
				d3.event.stopPropagation();
    		}
			if (!isNaN(downy)) {
			  redraw()();
			  downy = Math.NaN;
			  d3.event.preventDefault();
			  d3.event.stopPropagation();
			}
			if (dragged) { 
			  dragged = null; 
			}
  		}
	}

	var redraw = function() {
		var self = this;
		return function() {
			var tx = function(d) { 
		  		return "translate(" + self.x(d) + ",0)"; 
			},
			ty = function(d) { 
		  		return "translate(0," + self.y(d) + ")";
			},
			stroke = function(d) { 
		  		return d ? "#ccc" : "#666"; 
			};

			// Regenerate x-ticks…
			/*
			var gx = self.plotContainer.selectAll("g.x")
				.data(self.x.ticks(10), String)
				.attr("transform", tx);

			gx.select("text")
				.text(fx);

			var gxe = gx.enter().insert("g", "a")
				.attr("class", "x")
				.attr("transform", tx);

			gxe.append("line")
				.attr("stroke", stroke)
				.attr("y1", 0)
				.attr("y2", self.size.height);

			gxe.append("text")
				.attr("class", "axis")
				.attr("y", self.size.height)
				.attr("dy", "1em")
				.attr("text-anchor", "middle")
				.text(fx)
				.style("cursor", "ew-resize")
				.on("mouseover", function(d) { d3.select(this).style("font-weight", "bold");})
				.on("mouseout",  function(d) { d3.select(this).style("font-weight", "normal");})
				.on("mousedown.drag",  self.xaxis_drag())
				.on("touchstart.drag", self.xaxis_drag());

			gx.exit().remove();
			*/

			// Regenerate y-ticks…
			/*
			var gy = self.vis.selectAll("g.y")
				.data(self.y.ticks(10), String)
				.attr("transform", ty);

			gy.select("text")
				.text(fy);

			var gye = gy.enter().insert("g", "a")
				.attr("class", "y")
				.attr("transform", ty)
				.attr("background-fill", "#FFEEB6");

			gye.append("line")
				.attr("stroke", stroke)
				.attr("x1", 0)
				.attr("x2", self.size.width);

			gye.append("text")
				.attr("class", "axis")
				.attr("x", -3)
				.attr("dy", ".35em")
				.attr("text-anchor", "end")
				.text(fy)
				.style("cursor", "ns-resize")
				.on("mouseover", function(d) { d3.select(this).style("font-weight", "bold");})
				.on("mouseout",  function(d) { d3.select(this).style("font-weight", "normal");})
				.on("mousedown.drag",  self.yaxis_drag())
				.on("touchstart.drag", self.yaxis_drag());

			gy.exit().remove();
			*/
			self.plotGroup.call(d3.behavior.zoom().x(self.x).y(self.y).on("zoom", self.redraw()));
			update();    
		}  
	}

	var xaxis_drag = function() {
		return function(d) {
			document.onselectstart = function() { return false; };
			var p = d3.mouse(self.vis[0][0]);
			downx = x.invert(p[0]);
	  	}
	};

	var yaxis_drag = function(d) {
		var self = this;
	  	return function(d) {
			document.onselectstart = function() { return false; };
			var p = d3.svg.mouse(self.vis[0][0]);
			downy = y.invert(p[1]);
	  	}
	}
	
    d3.select("body")
      .on("mousemove.drag", mousemove())
      .on("touchmove.drag", mousemove())
      .on("mouseup.drag",   mouseup())
      .on("touchend.drag",  mouseup());

    redraw()();

}