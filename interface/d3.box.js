d3.functor = function functor(v) {
  return typeof v === "function" ? v : function() {
    return v;
  };
};

(function() {

d3.box = function(view_width, view_height, box_width) {
  var width = box_width,
      height = 1,
      duration = 0,
      x_domain = null,
      y_domain = null,
      value = Number,
      whiskers = boxWhiskers,
      quartiles = boxQuartiles,
      tickFormat = null;

  // For each small multiple…
  function box(g) {
    g.each(function(d, i) {
      var g = d3.select(this),
          min = d[1],
          max = d[2];

      // Compute quartiles. Must return exactly 3 elements.
      var quartileData = boxQuartiles(d);

      // Compute whiskers. Must return exactly 2 elements, or null.
      var whiskerData = boxWhiskers(d);

      // Compute the new x-scale.
      var x1 = d3.scaleLinear()
          .domain(x_domain.call(this, d, i))
          .range([0, view_width]);
          
      var y1 = d3.scaleLog()
          .domain(y_domain.call(this, d, i))
          .range([view_height, 1]);

      // Retrieve the old x-scale, if this is an update.
      /*
      var x0 = this.__chart__ || d3.scaleLinear()
          .domain([0, Infinity])
          .range(x1.range());
          
      var y0 = this.__chart__ || d3.scaleLinear()
          .domain([0, Infinity])
          .range(y1.range());
          */

      // Stash the new scale.
      this.__chart__ = x1;

      // Note: the box, median, and box tick elements are fixed in number,
      // so we only have to handle enter and update. In contrast, the outliers
      // and other elements are variable, so we need to exit them! Variable
      // elements also fade in and out.

      // Update center line: the vertical **BOX spanning the whiskers.
      var center = g.selectAll("rect.center")
          .data(whiskerData ? [whiskerData] : []);

      center.enter().insert("rect")
          .attr("class", "center")
          .attr("x", 0)
          .attr("height", function(d) { return y1(whiskerData[1]); })
          .attr("width", box_width)
          .attr("y", function(d) { return y1(whiskerData[0]) - y1(whiskerData[1]); })
          .style("fill-opacity", 0.5)
          .style("stroke", "none")
        .transition()
          .duration(duration)
          .attr("y", function(d) { return y1(whiskerData[1]); })
          .attr("height", function(d) { return y1(whiskerData[0]) - y1(whiskerData[1]); });

      center.transition()
          .duration(duration)
          .style("opacity", 1)
          .attr("y1", function(d) { return y1(whiskerData[0]); })
          .attr("y2", function(d) { return y1(whiskerData[1]); });

      center.exit().transition()
          .duration(duration)
          .style("opacity", 1e-6)
          .attr("y1", function(d) { return y1(whiskerData[0]); })
          .attr("y2", function(d) { return y1(whiskerData[0]); })
          .remove();

      // Update innerquartile box.
      var box = g.selectAll("rect.box")
          .data([quartileData]);

      box.enter().append("rect")
          .attr("class", "box")
          .attr("x", 0)
          .attr("y", function(d) { return y1(quartileData[2]); })
          .attr("width", box_width)
          .attr("height", function(d) { return y1(quartileData[0]) - y1(quartileData[2]); })
          .attr("stroke", "none")
        .transition()
          .duration(duration)
          .attr("y", function(d) { return y1(quartileData[2]); })
          .attr("height", function(d) { return y1(quartileData[0]) - y1(quartileData[2]); });

      box.transition()
          .duration(duration)
          .attr("y", function(d) { return y1(quartileData[2]); })
          .attr("height", function(d) { return y1(quartileData[0]) - y1(quartileData[2]); });

      // Update median line.
      var medianLine = g.selectAll("line.median")
          .data([quartileData[1]]);

      medianLine.enter().append("line")
          .attr("class", "median")
          .attr("x1", 0)
          .attr("y1", y1)
          .attr("x2", box_width)
          .attr("y2", y1)
        .transition()
          .duration(duration)
          .attr("y1", y1)
          .attr("y2", y1);

      medianLine.transition()
          .duration(duration)
          .attr("y1", x1)
          .attr("y2", x1);

      // Update whiskers.
      var whisker = g.selectAll("rect.whisker")
          .data(whiskerData || []);

      whisker.enter().insert("rect", "circle, text")
          .attr("class", "whisker")
          .attr("x1", -box_width/2.)
          .attr("y1", y1)
          .attr("x2", box_width/2.)
          .attr("y2", y1)
          .style("opacity", 1e-6)
        .transition()
          .duration(duration)
          .attr("y1", y1)
          .attr("y2", y1)
          .style("opacity", 1);

      whisker.transition()
          .duration(duration)
          .attr("y1", y1)
          .attr("y2", y1)
          .style("opacity", 1);

      whisker.exit().transition()
          .duration(duration)
          .attr("y1", y1)
          .attr("y2", y1)
          .style("opacity", 1e-6)
          .remove();

      // Compute the tick format.
      var format = tickFormat || x1.tickFormat(8);

/*
      // Update box ticks.
      var boxTick = g.selectAll("text.box")
          .data(quartileData);

	  // FIXME
      boxTick.enter().append("text")
          .attr("class", "box")
          .attr("dy", ".3em")
          .attr("dx", function(d, i) { return i & 1 ? 6 : -6 })
          .attr("x", function(d, i) { return i & 1 ? width : 0 })
          .attr("y", x1)
          .attr("text-anchor", function(d, i) { return i & 1 ? "start" : "end"; })
          .text(format)
        .transition()
          .duration(duration)
          .attr("y", x1);

      boxTick.transition()
          .duration(duration)
          .text(format)
          .attr("y", x1);

      // Update whisker ticks. These are handled separately from the box
      // ticks because they may or may not exist, and we want don't want
      // to join box ticks pre-transition with whisker ticks post-.
      var whiskerTick = g.selectAll("text.whisker")
          .data(whiskerData || []);

      whiskerTick.enter().append("text")
          .attr("class", "whisker")
          .attr("dy", ".3em")
          .attr("dx", 6)
          .attr("x", width)
          .attr("y", x1)
          .text(format)
          .style("opacity", 1e-6)
        .transition()
          .duration(duration)
          .attr("y", x1)
          .style("opacity", 1);

      whiskerTick.transition()
          .duration(duration)
          .text(format)
          .attr("y", x1)
          .style("opacity", 1);

      whiskerTick.exit().transition()
          .duration(duration)
          .attr("y", x1)
          .style("opacity", 1e-6)
          .remove();
          */
    });
    d3.timerFlush();
  }

  box.width = function(x) {
    if (!arguments.length) return box_width;
    width = x;
    return box;
  };

  box.height = function(x) {
    if (!arguments.length) return height;
    height = x;
    return box;
  };

  box.tickFormat = function(x) {
    if (!arguments.length) return tickFormat;
    tickFormat = x;
    return box;
  };

  box.duration = function(x) {
    if (!arguments.length) return duration;
    duration = x;
    return box;
  };

  box.x_domain = function(x) {
    if (!arguments.length) return x_domain;
    x_domain = x == null ? x : d3.functor(x);
    return box;
  };
  
  box.y_domain = function(x) {
    if (!arguments.length) return y_domain;
    y_domain = x == null ? x : d3.functor(x);
    return box;
  };

  box.value = function(x) {
    if (!arguments.length) return value;
    value = x;
    return box;
  };

  box.whiskers = function(x) {
    if (!arguments.length) return whiskers;
    whiskers = x;
    return box;
  };

  box.quartiles = function(x) {
    if (!arguments.length) return quartiles;
    quartiles = x;
    return box;
  };

  return box;
};

function boxWhiskers(d) {
  return [d[1], d[2]];
}

function boxQuartiles(d) { //lower, median, upper
  return [
    d[4],
    d[3],
    d[5]
  ];
}

})();