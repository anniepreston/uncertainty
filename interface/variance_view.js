VarianceView = function(){
	var width = 0.7*window.innerWidth;
	var height = 0.95 * window.innerHeight;
	var xAxisHeight = 60;
	var yAxisWidth = 60;
	var y_offset = 44;
	
	var zoomHeight = 0.9*height;
	var qualityHeight = 0.1*height;
	var chartHeight = zoomHeight;
	
	var currentColor;
	var colors = [];
	var storedZoomData = []; //FIXME: names not ideal...
	var storedNonZoomData = [];
	var activeIndices = [];
	var allIndices = [];
	var zoomMins = []; // y values
	var zoomMaxs = [];
	var nonZoomMins = [];
	var nonZoomMaxs = [];
	var x_min = Infinity;
	var x_max = -Infinity;
	
	var dataSource = 'hacc';
	var currentNorm = 'none';
	var currentScale = 'log';
	
	this.showOperation = function(event, frac, color){
    	addOperation(frac, color)
    }
    
    this.updateSource = function(event, source){
    	updateSource(source);
    }
		
	var zoomYAxis = d3.axisLeft();
		
	var zoomChart;

	var plotContainer = d3.select("body").append("svg")
	  .attr("width", width)
	  .attr("height", height);

	plotContainer.append("rect")
	  .attr("x", 0)
	  .attr("y", 0)
	  .attr("height", height)
	  .attr("width", width)
	  .attr("class", "border");
	  
	var chartGroup = plotContainer.append("g")
	  .attr("x", 0)
	  .attr("y", 0)
	  .attr("height", chartHeight)
	  .attr("width", width);

	 var qualityGroup = plotContainer.append("g")
		.attr("x", 0)
		.attr("y", 0)
		.attr("transform", function(){ return "translate(0," + (height-50) + ")"; })
		.attr("width", width)
		.attr("height", qualityHeight);
	
	// main view ("chart"):
	function addVariance(filename){
		console.log("filename: ", filename);
		allIndices.push(allIndices.length);
		activeIndices.push(allIndices[allIndices.length-1]);
		d3.csv(filename, function(error0, data0) {
            console.log("raw data: ", data0);
			var zoomData = new Array(data0.length);
			var nonZoomData = new Array(data0.length);
			//var x_var = 'x'; 
			var x_var = (dataSource == 'cmip' ? 'time' : 'mass'); //FIXME
            
            var y_min = Infinity,
		        y_max = -Infinity,
		        zoom_y_min = Infinity,
		        zoom_y_max = -Infinity;

			  pushData = function(d, idx, zoomArr, nonZoomArr){
			  		var x_val = parseFloat(d[x_var]),
			  		    x_bin = idx;
					//variance = parseFloat(d.variance),
					min = parseFloat(d.min),
					max = parseFloat(d.max),
					med = parseFloat(d.median),
					upper = parseFloat(d.upper),
					lower = parseFloat(d.lower);
					zoomArr[idx] = [x_val, min - med, max - med, 0, lower - med, upper - med, x_bin];
					nonZoomArr[idx] = [x_val, min, max, med, lower, upper, x_bin];
		
					if (x_val > x_max) x_max = x_val;
					if (x_val < x_min) x_min = x_val;
		
					if (max > y_max) y_max = max;
					if (min < y_min) y_min = min;
		
					if (max - med > zoom_y_max) zoom_y_max = max - med;
					if (min - med < zoom_y_min) zoom_y_min = min - med;
			  };
			  var idx = 0;
			  data0.forEach(function(x) {
			  	if (x.min != 0.0 || x.max != 0.0){
					pushData(x, idx, zoomData, nonZoomData);
					idx++;
				}
				else {
					zoomData.length = zoomData.length-1;
					nonZoomData.length = nonZoomData.length-1;
				}
			  });
			  			  
			  //FIXME: could be more elegant
			  storedZoomData.push(zoomData);
			  storedNonZoomData.push(nonZoomData);
			  zoomMins.push(zoom_y_min);
			  zoomMaxs.push(zoom_y_max);
			  nonZoomMins.push(y_min);
			  nonZoomMaxs.push(y_max);
			  
			  updateView();
		});
	}
	function updateView(){
		  
		  var y_min, y_max, mins = [], maxs = [];  
		  var currentData = [];
		  var currentColors = [];
		  
		  if (currentNorm == 'none') { 
		    for (var i = 0; i < activeIndices.length; i++){ 
		        currentData = currentData.concat(storedNonZoomData[activeIndices[i]]); 
		        mins.push(nonZoomMins[activeIndices[i]]);
		        maxs.push(nonZoomMaxs[activeIndices[i]]);
		    }
			y_min = Math.min.apply(Math, mins); 
			y_max = Math.max.apply(Math, maxs);
		  }
		  else { 
		    for (var i = 0; i < activeIndices.length; i++){ 
		        currentData.concat(storedZoomData[activeIndices[i]]); 
		        mins.push(zoomMins[activeIndices[i]]);
		        maxs.push(zoomMaxs[activeIndices[i]]);
		    }
			y_min = Math.min.apply(Math, mins);	
			y_max = Math.max.apply(Math, maxs);
		  }
		  console.log("current data: ", currentData);
		  
		  for (var i = 0; i < activeIndices.length; i++){
		    for (var j = 0; j < (currentData.length/activeIndices.length); j++){
		    	currentColors.push(colors[activeIndices[i]]);
		    }
		  } 
		  
		  //TEMP:
		  //y_min = 0;
		  //y_max = 31;
		  						  
		  var boxWidth = 0.7*(width - yAxisWidth)/(currentData.length/activeIndices.length);
		  var qualityData = Array.from({length:(currentData.length/activeIndices.length)}, () => Math.random());
					   
		  zoomChart = d3.box(width - yAxisWidth, chartHeight - y_offset, boxWidth)
			.whiskers(iqr());

		  zoomChart.x_domain([x_min, x_max]);
		  zoomChart.y_domain([y_min, y_max]);
		  zoomChart.width([width/currentData.length]);

		  xscale = d3.scaleLinear()
				  .domain([x_min, x_max])
				  .range([0, width - yAxisWidth - boxWidth/2.]);
				  
		  xBinScale = d3.scaleLinear()
				  .domain([0, currentData.length/activeIndices.length]) //FIXME
				  .range([0, width - yAxisWidth]);
				  
		  //FIXME: log/lin toggling
		  var zoomYScale = d3.scaleLog()
					.domain([y_min, y_max])
					.nice()
					.range([chartHeight - y_offset, 1])
					.nice();
		  /*
		  if (currentScale == 'lin') {
			  zoomYScale = d3.scaleLinear()
					.domain([y_min, y_max])
					.nice()
					.range([0.9*height-4, 40])
					.nice();
			}
		else if (currentScale == 'log'){
				zoomYScale = d3.scaleLog()
					.domain([y_min, y_max])
					.nice()
					.range([0.9*height-4, 40])
					.nice();
		}
		*/
				
		var xAxis = d3.axisBottom()
			.scale(xscale)
			.tickSize(10)
			.ticks(8)
			.tickFormat(d3.format(".1e"));

		var binXAxis = d3.axisBottom()
			.scale(xBinScale)
			.tickSize(10)
			.ticks(10);
			//.tickFormat(d3.format(".1e"));
	
		var zoomYAxis = d3.axisLeft()
			.scale(zoomYScale)
			.tickSize(10)
			.ticks(6);
			//.tickFormat(d3.format(".1e"));

		var centerArea = d3.area()
		  	.x(function(d) { return yAxisWidth + xscale(d[0]); })
		  	.y0(function(d) { return zoomYScale(d[5]); }) //upper
		  	.y1(function(d) { return zoomYScale(d[4]); }) //lower
			.curve(d3.curveCardinal.tension(0.5));

	  	var whiskerArea = d3.area()
			.x(function(d) { return yAxisWidth + xscale(d[0]); })
			.y0(function(d) { return zoomYScale(d[2]); }) //min
			.y1(function(d) { return zoomYScale(d[1]); }) //max
			.curve(d3.curveCardinal.tension(0.2));

		d3.select("box").selectAll("*").remove();
		d3.selectAll(".enter").remove();

/*
	 	var area_outer = chartGroup.selectAll(".outer")
	 		.data([currentData])
	 		.enter().append("path")
	 			.attr("d", whiskerArea)
	 			.attr("class", "outer")
	 			.attr("fill", function(d,i) { return currentColors[i]; })
		 	 	.attr("transform", function(){ return "translate(0," + y_offset + ")"; })
	 			.attr("fill-opacity", "0.2");
	 			
		var area_middle = chartGroup.selectAll(".middle")
	 	 	.data([currentData])
	 	 	.enter().append("path")
	 	 	  .attr("d", centerArea)
	 	 	  .attr("class", "middle")
	 	 	  .attr("fill", function(d,i) { return currentColors[i]; })
	 	 	  .attr("transform", function(){ return "translate(0," + y_offset + ")"; })
	 	 	  .attr("fill-opacity", "0.3");
*/

		var zoomBars = chartGroup.selectAll("box")
			.data(currentData);
			
		zoomBars.attr("class", "update");
			
		zoomBars.enter()
			.append("svg")
			.attr("class", "enter box")
			.attr("x", function(d, i) {console.log("d: ", d); return yAxisWidth + xBinScale(d[6]) - (boxWidth/2.);}) //FIXME: bin/not bin
			.attr("y", y_offset)
			.attr("fill", function(d,i) { return currentColors[i]; })
			.attr("stroke", function(d,i) { return currentColors[i]; })
			.attr("width", width)
			.attr("height", chartHeight - y_offset)
			.call(zoomChart);
			  
		zoomBars.exit().remove();
	 
		chartGroup.append("g")
			  .attr("transform", function(){ return "translate(" + yAxisWidth + "," + (zoomHeight) + ")"; })
			  .call(binXAxis);

		chartGroup.append("g")
		      .attr("height", chartHeight)
			  .attr("width", 60)
			  .attr("transform", function(){ return "translate(" + (yAxisWidth-20) + "," + y_offset + ")"; })
			  .call(zoomYAxis);
			  
		plotContainer.append('text')
		  	.attr('class', 'label')
		  	.text('avg. speed (m/s)')
		  	.attr('transform', 'translate(12,352),rotate(270)');
		
		plotContainer.append('text')
		  	.attr('class', 'label')
		  	.text('distance (m)')
		  	.attr('transform', 'translate(512,712)');
			
		  qualityGroup.selectAll("g")
			.data(qualityData)
			.enter()
			.append("rect")
			.attr("x", function(d,i){ return (boxWidth+5.7)*i;})
			.attr("y", 20)
			.attr("transform", "translate(60,0)")
			.attr("width", boxWidth)
			.attr("height", boxWidth)
			.attr("fill", function(d){ return d3.interpolateGreys(d + 0.2);}) //FIXME: fill with real data
			.on("mouseover", showConvergence)
			.on("mouseout", hideConvergence); //FIXME: also fill with real data
	}
	function iqr() {
	  return function(d, i) {
		var q1 = d[4],
			q3 = d[5];
		return [q1, q3];
	  };
	}

   var update = function() {
	   var lines = d3.select("body").select("path").attr("d", this.line(this.points)); //FIXME: for when we have grid axes
	}
		
	function addOperation(frac, color){
		zoomChart = null;
		delete zoomChart;
		var fn;
		//fn = 'data/traffic_data_40_bins/variance_t_26_size_1.0.csv';
		if (dataSource == 'cmip'){ //FIXME
			fn = 'data/' + dataSource + '/CMIP_variance_N_200_frac_' + frac + '_adjusted.csv';
		}
		else {
			var suffix = '_0.1_130-150.csv';
			fn = 'data/' + dataSource + '/Variance_Average_' + frac + suffix;
		}
		//FIXME!!!
		currentColor = color;
		//currentColor = '#707070';
		colors.push(currentColor);
		plotContainer.selectAll(".checkbox")
			.data(colors)
			.enter()
			.append("rect")
			.attr("class", "checkbox")
			.attr("x", function(d,i){ return 6 + i * 24; })
			.attr("y", 6)
			.attr("width", 20)
			.attr("height", 20)
			.attr('stroke-width', 2)
			.attr("stroke", function(d, i){ return d; })
			.attr("fill", function(d,i){ return d; })
			.attr("fill-opacity", 0.6)
			.on("click", toggleCheckbox);
		
		console.log("loading variance from file " + fn);
		addVariance(fn);
		
	}
	//FIXME
	const varChartSize = 80;
	
	var xConvScale = d3.scaleLinear()
		.domain([1,20])
		.range([0,varChartSize])
	var yConvScale = d3.scaleLinear()
		.domain([0,1])
		.range([0,varChartSize]);
	
	var lineFunction = d3.line()
	  .x(function(d) { return xConvScale(d.x); })
	  .y(function(d) { return yConvScale(1-d.y); });
	  //.curve('linear');
	function showConvergence(d,i){
			
		//FIXME: PLACEHOLDER
		var convergenceData = [ //this is fake
		{'y': 0.2, 'x': 1},
		{'y': 0.4, 'x': 2},
		{'y': 0.3, 'x': 3},
		{'y': 0.5, 'x': 4},
		{'y': 0.4, 'x': 5},
		{'y': 0.45, 'x': 6},
		{'y': 0.34, 'x': 7},
		{'y': 0.6, 'x': 8},
		{'y': 0.24, 'x': 9},
		{'y': 0.55, 'x': 10},
		{'y': 0.7, 'x': 11},
		{'y': 0.51, 'x': 12},
		{'y': 0.43, 'x': 13},
		{'y': 0.53, 'x': 14},
		{'y': 0.5, 'x': 15},
		{'y': 0.6, 'x': 16},
		{'y': 0.54, 'x': 17},
		{'y': 0.53, 'x': 18},
		{'y': 0.52, 'x': 19},
		{'y': 0.53, 'x': 20}];
		d3.selectAll(".varChart").remove();
		var x = parseFloat(d3.select(this).attr("x"));
		if (x > width - yAxisWidth - varChartSize) x -= varChartSize;
		console.log("x: ", x);
		var chartBox = d3.select(this.parentNode).append("rect")
			.attr("class", "varChart")
			.attr("transform", function(){ return "translate(" + (x + yAxisWidth) + "," + -(varChartSize - 12) + ")"; })
			.attr("width", varChartSize)
			.attr("height", varChartSize)
			.attr("stroke", "#707070")
			.attr("fill", "white")
			.attr("stroke-width", "2px");
		var chart = d3.select(this.parentNode).append("path")
			.data([convergenceData])
			.attr("transform", function(){ return "translate(" + (x + yAxisWidth) + "," + -(varChartSize - 12) + ")"; })
			.attr("class", "line varChart")
			.attr("d", lineFunction)
			.attr('stroke', '#707070')
			.attr('stroke-width', '2px')
			.attr('fill', 'none');
	}
	function hideConvergence(d,i){
		d3.selectAll(".varChart").remove();
	}
	
	function updateSource(source){
		console.log("updating source in variance view: ", source);
		dataSource = source;
		d3.selectAll('.enter').remove();
	}
	
	function toggleCheckbox(d,i){
		if (d3.select(this).style("fill-opacity") == 0.0){
			d3.select(this).style("fill-opacity", 0.6);
			activeIndices.push(i);
		}
		else {
			d3.select(this).style("fill-opacity", 0.0);
			var index = activeIndices.indexOf(i);
			if (index > -1) {
				activeIndices.splice(index, 1);
			}
		}
		updateView();
	}
}
