var width = window.innerWidth;
var height = window.innerHeight;

const data_view = new DataView("data");
//var plot_view = new PlotView("chart");
const train_view = new TrainView("train");
//const plot_view = new PlotCompareView("chart");
const var_view = new VarianceView("chart");

EventBus.addEventListener("addSampleEvent", train_view.updateFeatures, train_view);
EventBus.addEventListener("operationEvent", var_view.showOperation, var_view);
EventBus.addEventListener("operationEvent", train_view.showOperation, train_view);
EventBus.addEventListener("sourceEvent", var_view.updateSource, var_view);