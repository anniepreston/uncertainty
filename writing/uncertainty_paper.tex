% $Id: template.tex 11 2007-04-03 22:25:53Z jpeltier $

%\documentclass{vgtc}                          % final (conference style)
\documentclass[review]{vgtc}                 % review

%\documentclass[widereview]{vgtc}             % wide-spaced review
%\documentclass[preprint]{vgtc}               % preprint
%\documentclass[electronic]{vgtc}             % electronic version

%% Uncomment one of the lines above depending on where your paper is
%% in the conference process. ``review'' and ``widereview'' are for review
%% submission, ``preprint'' is for pre-publication, and the final version
%% doesn't use a specific qualifier. Further, ``electronic'' includes
%% hyperreferences for more convenient online viewing.

%% Please use one of the ``review'' options in combination with the
%% assigned online id (see below) ONLY if your paper uses a double blind
%% review process. Some conferences, like IEEE Vis and InfoVis, have NOT
%% in the past.

\ifpdf%                                % if we use pdflatex=
  \pdfoutput=1\relax                   % create PDFs from pdfLaTeX
  \pdfcompresslevel=9                  % PDF Compression
  \pdfoptionpdfminorversion=7          % create PDF 1.7
  \ExecuteOptions{pdftex}
  \usepackage{graphicx}                % allow us to embed graphics files
  \DeclareGraphicsExtensions{.pdf,.png,.jpg,.jpeg} % for pdflatex we expect .pdf, .png, or .jpg files
\else%                                 % else we use pure latex
  \ExecuteOptions{dvips}
  \usepackage{graphicx}                % allow us to embed graphics files
  \DeclareGraphicsExtensions{.eps}     % for pure latex we expect eps files
\fi%

\usepackage{mathptmx}
\let\ifpdf\relax
\usepackage{graphicx}
\usepackage{times}
\usepackage{url}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{footmisc}
\usepackage{array}
\usepackage{enumitem}
\usepackage{makecell}
\usepackage{caption}

\renewcommand\theadalign{cb}
\renewcommand\theadfont{\bfseries}
\renewcommand\theadgape{\Gape[4pt]}
\renewcommand\cellgape{\Gape[4pt]}


\usepackage{color, colortbl}
\newcommand{\pin}[1]{{\color{red} #1}}
\newcommand{\pinfixed}[1]{{\color{blue} #1}}

\graphicspath{{./images/}}

%% If you are submitting a paper to a conference for review with a double
%% blind reviewing process, please replace the value ``0'' below with your
%% OnlineID. Otherwise, you may safely leave it at ``0''.
%%\onlineid{0}

%% declare the category of your paper, only shown in review mode
\vgtccategory{Research}

%% allow for this line if you want the electronic option to work properly
\vgtcinsertpkg

%% In preprint mode you may define your own headline.
%\preprinttext{To appear in an IEEE VGTC sponsored conference.}

%% Paper title.

\title{Modeling Simulation Uncertainty for (Interactive?) Visual Analysis}

%% This is how authors are specified in the conference style

%% Author and Affiliation (single author).
%%\author{Roy G. Biv\thanks{e-mail: roy.g.biv@aol.com}}
%%\affiliation{\scriptsize Allied Widgets Research}

%% Author and Affiliation (multiple authors with single affiliations).
%%\author{Roy G. Biv\thanks{e-mail: roy.g.biv@aol.com} %
%%\and Ed Grimley\thanks{e-mail:ed.grimley@aol.com} %
%%\and Martha Stewart\thanks{e-mail:martha.stewart@marthastewart.com}}
%%\affiliation{\scriptsize Martha Stewart Enterprises \\ Microsoft Research}

%% Author and Affiliation (multiple authors with multiple affiliations)
%%\author{Annie Preston\thanks{e-mail:\{apreston, klma\}@ucdavis.edu} 
%%\qquad Kwan-Liu Ma\footnotemark[1]\\%
%%\scriptsize University of California at Davis
%%}

%% A teaser figure can be included as follows, but is not recommended since
%% the space is now taken up by a full width abstract.
%\teaser{
%  \includegraphics[width=1.5in]{sample.eps}
%  \caption{Lookit! Lookit!}
%}

%% Abstract section.

\abstract{
later... 
} % end of abstract

%% ACM Computing Classification System (CCS). 
%% See <http://www.acm.org/class/1998/> for details.
%% The ``\CCScat'' command takes four arguments.

\CCScatlist{ 
  %\CCScat{I.6.4}{Simulation and Modeling}{Simulation Output Analysis}{};
  %\CCScat{J.2}{Physical Sciences and Engineering}{Astronomy}{};
  %\CCScat{J.2}{Physical Sciences and Engineering}{Earth and atmospheric sciences}{};
}

%% Copyright space is enabled by default as required by guidelines.
%% It is disabled by the 'review' option or via the following command:
% \nocopyrightspace

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%% START OF THE PAPER %%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
\maketitle
\section{Introduction}
	While computers can generate terabytes of scientific simulation data in days, domain-specific data analysis tasks take much longer, causing a simulation analysis bottleneck~\cite{dasgupta}. Domain-specific challenges, such as particular data formats, multi-dimensionality, domain-specific definitions of uncertainty, and in-depth knowledge required to create analysis tools, add to this bottleneck. Many goals, though, are common to almost all scientists analyzing simulations: understanding how initial conditions and parameterizations affect observable quantities, their uncertainties, and their relationship with experimental data is key to answering questions \cite{dasgupta}. Interactively seeing the results of these choices is ideal. However, uncertainty in simulation data is especially difficult to explore with conventional visualization tools \cite{bonneau}, leading to a lack of software that allows ``rapid, dynamic exploration of different hypotheses in which the system adapts to the interaction'' \cite{dasgupta}. This is exacerbated by the large size of simulation data and the long time it takes to calculate uncertainty with typical methods.

A range of factors creates uncertainty in scientific simulations, including unknown input conditions, unknown external factors, internal variability due to complex physical processes, and differences in the algorithmic formulations of these processes \cite{Deser2012}, \cite{georgakakos}, \cite{haroz:2008}, as well as uncertainty in observations against which simulations are compared \cite{gleckler}. A  fundamental, underlying cause of uncertainty in simulations is discretization, sometimes referred to as ``discreteness noise'' \cite{rau}. When continuous data, such as temperatures or mass distributions, are modeled with discrete representations and sampled in discrete bins or timesteps, some information about the modeled, continuous system is lost, creating uncertainty about measurements of these data. 

N-body simulations of dark matter, for example, model regions of varying mass density using discrete ``particles.'' Measurements of this simulated dark matter, then, might give different results depending upon the details of the discretization. This is also true in climate models, which provide quantities at discrete spatial and temporal intervals. Additionally, scientists typically study ensembles of climate models; the choice of which models to include in an ensemble is another source of discretization and noise. It is important to understand how these uncertainties manifest in various measurements of simulation data. Calculating this information, though, can be quite time-consuming. We aim to develop a method for estimating this ``discreteness noise'' by taking small data samples, allowing rapid visual exploration of uncertainty under various conditions.

\section{Related Work}
Several thorough overviews of the state of uncertainty visualization exist, such as \cite{bonneau}, (...). These summaries underscore a general neglect for uncertainty in visualization research; certainly, many approaches have been proposed, but uncertainty is generally excluded from visualizations and is often seen as an afterthought, though in fact it is crucial to good scientific analysis. More work has focused on visual representations of uncertainty than on elucidating sources of uncertainty with visualization, and in particular, quantifying and modeling uncertainty is difficult and has been neglected~\cite{bonneau}.

Visual analytics is inseparable from the problem of uncertainty: data gathering, manipulation, visualization, and user perception all introduce uncertainties which users need to understand in order to properly draw conclusions from data \cite{sacha}. Researchers have created uncertainty-aware visual analytics frameworks, such as node-link diagrams enhanced with glyphs indicating uncertainty \cite{liu}. To visualize uncertainty propagation through a series of data transformations, the authors of \cite{correa2009framework} use Gaussian mixture models to model ``aggregated uncertainty'' [uncertainty resulting from what?] from these transformations, enabling visualizations that show the variability in derived data with scatter plots and covariance matrices. The authors of \cite{wu} expand on this work, adding ellipsoids representing covariance for particular variables and flow trees to show the relative uncertainty introduced with each data transformation choice. Uncertainty can be visualized using blurring effects \cite{haroz:seeing}, others?. Visualizing uncertainty in physical space, such as in bounded 3-dimensional volumes, is challenging, as it can occlude other variables, and users are not used to seeing uncertainty depicted in three dimensions as they are in two \cite{haroz:2008}, \cite{li}.

Uncertainty in scientific data compounds on the challenges in uncertainty visualization, given its complexity and multidimensionality. Commonly, simulation uncertainty is analyzed by considering the results from a group, or ensemble, of simulation runs, perhaps with slightly perturbed initial conditions. The authors of \cite{potter:2009} use a variety of methods for visualizing this uncertainty in a climate model ensemble, using both spatial views (color on a map indicating standard deviation in that location; contours indicating probability levels; spaghetti plots of possible trajectories) and two-dimensional views (quartile trend charts; plume trend charts). Spaghetti plots can also be combined with glyphs representing uncertainty, and these plots can also be reimagined as graduated ribbons as in \cite{sanyal}. However, most climate ensembles do not have enough member models to support nuanced characterizations of their underlying uncertainty in this way \cite{potter:2009}. Color can also indicate uncertainty \cite{potter:2009}, especially in 3D space, as can ellipsoids in 2D or 3D, or ``cones'' showing possible trajectories \cite{li}; the authors also employ ``magic glasses,'' allowing users to show or hide uncertainty information on top of the original data. These techniques may be more useful for expressing qualitative properties of data uncertainty than for precise quantitative analysis.

Quantitatively modeling uncertainty in physical data, such as those from scientific simulations, introduces more challenges. For example, temporal downsampling introduces uncertainty to values that are interpolated across timesteps. To address uncertainty in particle paths arising from interpolation between downsampled time steps, \cite{chen2015uncertainty} developed a polynomial-based error modeling method, using Bezier curve fitting to reconstruct physically plausible paths and visualize the results. Categorization of features, such as vortices, in simulations is often a binary choice, meaning that including uncertainty information in these classifications is difficult. The authors of \cite{biswas2015uncertainty} use fuzzy inputs and a consensus-based visualization tool to improve vortex classification in simulations. A possible approach to modeling uncertainty from a simulation is to train a Gaussian process emulator to model a simulation using only its most influential input parameters, allowing rapid prediction of uncertainty \cite{gomez2014dissecting}. One can also use Bayesian Model Averaging to visualize the predictive uncertainty of model ensembles and individual models, as in~\cite{gosink}. Instead of calculating uncertainties and visualizing the result, representative sampling can be used to display uncertainty information to avoid occlusion while maintaining statistical properties \cite{liu2017uncertainty}.

\section{Methods}
We consider two target applications on which to test our uncertainty modeling method. For each application, we consider a source dataset and a calculation of interest. The result of this calculation involves inherent uncertainty because of the discretization of the underlying data. Our aim is to estimate the uncertainty in this calculation based on taking very small samples of these datasets and applying the calculation to those, then training a model.

\subsection{Data}
\subsubsection{Dark Matter Simulations}
We use data from HACC \cite{habib2016hacc}, a dark matter simulation framework. In N-body cosmological simulations, dark matter mass distributions are represented by discrete particles. This representation discretely approximates the distribution of dark matter in the universe, which leads to uncertainty, known here as ``particle shot noise.'' Shot noise inserts uncertainty into all analyses of dark matter simulations.

Dark matter halos, which are gravitationally bound groups of dark matter particles that coalesce over time, are important features to study, partly because they are the environments in which galaxies form.  Algorithms that identify catalogs of halos from time-variant dark matter simulation data are called ``halo finders.'' A wide range of halo finders exists; assessing the quality of their performance is difficult, because there is no clear definition of ``halo,'' and because halos can have complex structures~\cite{behroozi}. However, the results from a halo finder determine the accuracy and scientific usefulness of properties derived from halo catalogs. 

The ``halo mass function'' (HMF), one of these derived measurements, is a histogram of the mass distribution of dark matter halos within a simulation. Particle shot noise uncertainty, which means that the assignment of particles to particular halos is uncertain, leads to uncertainty in the HMF.  Small differences in halo finder results may result in significant differences to the resulting HMFs and to ``merger trees,'' which are  \cite{avila2014sussing}. (For more information on dark matter, halos, and merger trees, see~\cite{lacey1992}.)

As new sky surveys and telescopes measure cosmological properties to ever-greater precision, the need for accurate measurements from cosmological simulations, and understanding of their bias and uncertainty, becomes even more urgent. For comparison with sky surveys, HMFs measured from simulations need to be accurate to within 1-5 percent~\cite{behroozi},~\cite{tinker}. In order to use the HMF to observationally probe dark matter, cosmologists must continue identifying and reducing uncertainties in theoretical HMF measurements~\cite{murray2013}.
          
\subsubsection{Climate Models}
We also use an ensemble of climate models from Phase 5 of the Coupled Model Intercomparison Project (CMIP5)~\cite{taylor2012overview}. Climate models are highly complex, including hundreds of tunable parameters determining the behavior of interdependent physical processes \cite{debusschere}, \cite{randall}. We selected an ensemble of 12 models (see Table 1). Models are run according to scenarios, called ``Representative Concentration Pathways'' (RCPs), which are possible trajectories for future greenhouse gas emissions. All models in our ensemble represent RCP4.5, which is a scenario of future low-emissions climate policy~\cite{van2011representative}. These RCPs provide a manageable, intuitive set of projected outcomes to analysts and policymakers~\cite{mcsweeney}.

The choice of models making up the ensemble is a source of uncertainty.  Most often, climate scientists measure quantities and patterns by averaging over an ensemble of models (``multi-model mean'')~\cite{vavrus}. Uncertainty, then, is defined by the spread of models around this mean \cite{deser}, \cite{georgakakos}.  Information about the spread in the models, such as their standard deviation, can help quantify this uncertainty. But the choice of models to include in an ensemble is arbitrary and also contributes to uncertainty \cite{knutti}, \cite{vavrus}. For example, results may significantly change with the inclusion of one model in place of another. If a particular model is in disagreement with the consensus of most other models, it is unclear whether to include or exclude this model: its outlier status does not necessarily mean it is wrong, and it may well be within the range of plausible outcomes \cite{mcsweeney}. Several approaches to quantifying this uncertainty due to model choice are outlined in \cite{vavrus}. The authors choose to strike a balance between simple and complex methods using bootstrapping (see below). They demonstrate that the mean and standard deviation of a bootstrapped, resampled climate model ensemble provide precise information about the confidence level of a prediction. Bootstrapping the ensemble allows for far more nuanced uncertainty analysis than does simply considering the range of values in the models themselves~\cite{potter:2009}. There may be scientific reasons for excluding a particular model from the ensemble--if it does not realistically represent a key, known climate process--but that analysis is outside the scope of our study; our results may be applied to any group of models with any selection criteria. 

We choose ocean heat content (OHC) for analysis because of its conceptual importance: ocean heat content is a key indicator of climate change effects \cite{cheng}. It is also mathematically simple: calculating OHC requires integrating heat capacity over every ocean grid cell volume in a model. Therefore, we selected our models from among those CMIP5 models with ocean cell area and ocean heat capacity data. Some models have different spatial resolutions, adding another source of discretization uncertainty. It is important to assess and constrain uncertainty in OHC, as it is closely related to Earth's heat balance, which is crucial to the conclusions of climate change studies ~\cite{cheng},~\cite{gregory}. 
     
\noindent\begin{tabular}{@{} l c c }
\thead{Modeling Center/Group} & \thead{Model Name} \\
\hline
\small \makecell{Canadian Centre for Climate Modelling and Analysis} & \small \makecell{CanCM4 \\ CanESM2} \\
\small \makecell{National Center for Atmospheric Research} & \small CCSM4 \\
\small \makecell{Centre National de Recherches Météorologiques; \\ Centre Européen de Recherche et Formation \\\ Avancée en Calcul Scientifique} & \small CNRM-CM5 \\
\small \makecell{NASA Goddard Institute for Space Studies} & \small GISS-E2-H-CC \\
\small \makecell{Met Office Hadley Centre} & \small HadGEM2-ES \\
\small \makecell{Institut Pierre-Simon Laplace} & \small IPSL-CM5A-LR \\
\small \makecell{Atmosphere and Ocean Research Institute; \\ National Institute for 
Environmental Studies;\\ Japan Agency for Marine-Earth Science and Technology} & \small \makecell{MIROC4h \\ MIROC-ESM-CHEM }\\
\small \makecell{Max-Planck-Institut f{\"u}r Meteorologie} & \small MPI-ESM-LR \\
\small \makecell{Meteorological Research Institute} & \small MRI-CGCM3 \\
\small \makecell{Norwegian Climate Centre} & \small NorESM1-ME \\
\end{tabular}\\
\textbf{Table 1.} Model and group names for the ensemble of twelve CMIP5 models that make up our test ensemble. 
\bigskip

\subsection{Bootstrapping}
Bootstrapping describes randomly sampling a dataset with replacement; that is, randomly choosing elements of a dataset and allowing elements to be chosen multiple times. Some elements, then, will be included more than once, and some will be excluded. One can use many iterations of bootstrapping to create a dataset useful for estimating properties of the original dataset, such as uncertainty. Thus, bootstrapping provides a simple, generalizable, powerful way to quantify uncertainty. 

A bootstrapping technique has been used in~\cite{rau} to estimate the effect of particle noise in N-body simulations. Researchers use N-body simulations to study gravitational lensing, a phenomenon in which light is bent, or ``lensed,'' around a massive object in space. The authors of~\cite{rau} create a dataset with 100 bootstrapped resamplings, each containing the same number of particles as the original dataset; this allows them to test the effect of particle noise on the lensing in the simulation.

Bootstrapping to estimate uncertainty in quantities from climate models has been performed and tested~\cite{vavrus}. As discussed above, the multi-model mean used to assess climate ensembles introduces uncertainty. The authors use bootstrapping, resampling from their 13 ensemble models, to assess the uncertainty of temperature and precipitation measurements.

``Partitioning'' uncertainties in climate simulation data based on their sources is quite difficult; Bayesian models can be used to determine the contribution of each source of uncertainty~\cite{northrop}, but in general, the complexity of uncertainties in interacting processes can occlude one another. Bootstrapping over the choice of models provides a conceptually elegant way to isolate uncertainty resulting from model choice from the other sources of uncertainty. This is true, too, for particle noise; bootstrapping will allow us to quantify and differentiate the uncertainty resulting from particle noise.

\subsection{Data Processing}
Our technique is based on bootstrapping small samples of our datasets, with $N_{\mathrm{subset}} \ll N_{\mathrm{particles}}$, where $N_{\mathrm{particles}}$ represents the number of particles (or the domain-specific equivalent) in the original dataset. We aim to measure the uncertainty in observations of these subsampled datasets, then create a model to predict the uncertainty in the full dataset from these samples. To create a bootstrapped dataset for dark matter simulation data, we set a fraction $f$, then select a subset of $f * N_{\mathrm{particles}}$ samples, with replacement, for each bootstrapping iteration. For the climate ensemble, at each bootstrapping iteration, we select a set of the models, with $N_{\mathrm{bootstrap}} = N_{\mathrm{models}}$, as in~\cite{vavrus}. We then select a subset of $f * N_{\mathrm{cells}}$ cells from each of the models in the set. (TO DO: are we also randomly selecting cells each time, or should they be the same cells?)

We determine a number of domain bins, sample points, etc., for each dataset, or assign an algorithm to automate this decision. This is straightforward for the climate model data, which in our case is divided into monthly timesteps. In our first modeling attempt with the dark matter data, we set uniformly spaced mass bins. In order to ``smooth'' our input data for improved modeling---so as not to overfit the data---we set the number of bins to half as many as were automatically chosen by the yt halo finding algorithm \cite{yt}. This provided good spacing for larger mass bins, with smoothly varying values between mass bins at various sampling sizes. For the smaller mass bins, however, the measurements---i.e., the number of halos larger than these masses---had high values that varied greatly between adjacent mass bins and were wildly inconsistent depending on the sample size. Thus, we use adaptive mass binning (TO DO.)

\subsection{Uncertainty Measurement}
Next, we perform the relevant operation on each bootstrapped dataset. To obtain halo catalogs for each dark matter subset, we modify the yt halo-finding library to find halo measurements at our specified mass bins (TO DO: specify options)~\cite{yt}. This yields, for each bootstrapping iteration, a value for halo number density at each mass bin, which is the halo mass function (HMF). 

For the climate data, we measure ocean heat content (OHC) for each bootstrapping iteration by integrating over the heat capacity for each ocean cell, then averaging these values over the ensemble, for each timestep. This yields, for each bootstrapping iteration, an OHC value for each timestep.

For each of these measurements, we check the variance in each timestep or bin by plotting the variance per iteration and measuring the threshold of variance (i.e., the number of iterations N after which the range of values is within some value $\epsilon$). Once we have calculated the OHC or HMF over enough iterations to have reached convergence in most mass bins or timesteps, we write a model input file including the bin/timestep, variance, minimum, maximum, and median values, and 25th and 75th quartiles. These comprise the five summary statistics depicted in boxplots~\cite{brodlie2012review}. 

Some of these quantities change with varying subset sizes, such as mass, halo number density, and the magnitude of ocean heat content, while some, such as timesteps, don't. Downsampling the data means that though the variance of a quantity in a subset may be roughly proportional to the variance of that quantity in the full dataset, the absolute values will not be the same. For example, when we downscale, the range of halo masses decreases. For our model, we'd like to convert between these masses to the masses found in the full dataset. This will be true of any property that is directly affected by downsampling, such as number densities. Ideally, this scaling can be hidden within the model, which will learn the relationship between input and output values. However, we may have to explicitly scale values if the models do not address this adequately (TO DO).

FOR CMIP: mention that we have to scale based on the relative ocean area included in each subsample.

\begin{figure}[h]
    \centering
    \includegraphics[width=.45\textwidth]{images/uncertainty_scheme}
    \caption{A (PLACEHOLDER!) schematic diagram of our uncertainty modeling approach. After training a model with several subsampled, bootstrapped datasets, we can input new downsampled data and yield a prediction of uncertainty. This prediction can be compared against ground truth data for our own validation.}
     \label{scheme}
\end{figure}

\subsection{Modeling}
We chose regression because regression allows one to predict the value of responses, given predictors, and estimate the association among predictors and responses. Our predictors will be the uncertainty properties of a small data subset, and our responses will be the predicted uncertainty properties of the full dataset. We explored whether to use parametric or non-parametric regression. Parametric regression fits a functional form to the model inputs and outputs. This would be conceptually elegant for inserting new data into the model inputs and calculating the outputs. However, choosing a functional form to fit will be difficult without intuition for what shape the uncertainties of a dataset should take. Nonparametric modeling has the advantage of more degrees of freedom, sacrificing the elegance of a functional form---which may not be meaningful with these scientific data---for greater predictive power. TO DO: if we settle on kernel regression, explain why. 

We test and compare three nonparametric modeling approaches:

\begin{itemize}
\item \textbf{``Ground Truth:''} This option serves as a baseline that we hope to vastly outperform in efficiency. In this scheme, we train a model with ground truth data as the training ``labels,'' and a small subset of a fixed size as the training input. To test, we will insert another dataset at the given fraction and compare its predictions with the ground truth uncertainty. This should provide good accuracy, and is conceptually clear, but it will take a very long time to generate the ground truth training data, and we cannot input data with a new subset size without retraining.
\item \textbf{``Train-Each-Time:''} Using two or more subsets, we train a new model for each set of input conditions, etc. This way, we learn a relationship between the input mass bin and fraction with the output variance. With this approach, it may be difficult to incorporate scaling information (see above) in the model. It should involve relatively simple regression given its few inputs, but retraining each time will likely be tedious.
\item \textbf{``Multi-Scale Model:''} This should be the most versatile method, but the models may take the longest to train. For training input, we assemble samples from pairs of subsets, so that the model learns a relationship between the input variables and fraction size, allowing the user to specify an output fraction size for which to estimate the uncertainty. For actual use, the output fraction size will be 1.0, representing a query for the uncertainty of the full dataset. This method will provide the most utility for scientific workflows, such as substituting a new subset generated with different initial conditions and predicting its uncertainty.
\end{itemize}

\noindent\begin{tabular}{@{} c  c  c  c }
\thead{method} & \makecell{\textbf{Ground}\\\textbf{Truth}} & \makecell{\textbf{Train-}\\\textbf{Each-Time}} & \makecell{\textbf{Multi-Scale}\\\textbf{Model}} \\
\hline
\thead{functional\\ form} & $V_{1.0} = M(V_f, m)$ & $V = M(m, f)$ & $M(m, v_i, f_i, f_o)$ \\
\thead{training\\input} & $\{m_i, v_i\}$ & $\{m_i, f_i\}$ & $\{m_i, f_i, f_o, v_i\}$ \\ 
\thead{training\\output} & $V_{1.0}$ & $V_{i}$ & $V_o$ \\
\end{tabular}
\textbf{Table 2.} Mathematical descriptions of the three modeling options. $V_{1.0}$ represents the variance of the full dataset (i.e., $f = 1.0$); $V_{i}$ represents variance corresponding to an input fraction $f_i$; and $V_{o}$ represents variance corresponding to an output fraction $v_o$.
\bigskip

We choose to compare two options for measuring the uncertainty of a sample: a single quantity (variance), or a five-figure summary (minimum, 25th percentile, median, 75th percentile, maximum). The former has the advantage of simplicity, requiring learning surfaces of only a few dimensions with regression, while the latter is much more scientifically useful, including predicting the value of the measurement itself (here, the median) in addition to its uncertainty.

TO DO: Once we have settled on a particular modeling method, describe it here, including our gridding/stepping approach.

TO DO: Create a new/better diagram describing the workflow here.

\subsection{Validation}

Our simplest form of validation will be to measure the ground truth variance for each dataset, then compare our models' predictions and their root mean square error distances from the ground truth.

(there are a couple of other methods I might use - TO DO.)

The authors of~\cite{rau} use a bootstrapping test to check that the properties of the dataset are consistent after their bootstrapping procedure. We performed the same test -- TO DO.

\subsection{Visualization and Interface}
The high level of precision in modern cosmological simulations, meaning very small uncertainties, is a particular challenge for visualization. These tiny ratios are difficult to convey qualitatively without a strong quantitative component. Additionally, halo masses and number densities span several orders of magnitude, meaning that uncertainties will also span several orders of magnitude. The authors of ~\cite{li} provide suggestions for how to address these challenges. We provide logarithmic scaling and ... (TO DO).

TO DO: include an interface diagram here!

We provide three views: a data view, a model view, and an uncertainty view. In the data view, we allow the user to specify data for model training and also for model input; this includes choosing an input domain, input parameters, and parameters of the operation. In the model view, we allow the user to specify the modeling method, output type, and training subset sizes, as well as a history of already-trained models. The uncertainty view shows results, including a boxplot representation of the predicted uncertainty for a given operation, and the relative confidence in these results. We also show convergence of the variance of bootstrapped samples in real time, stopping either once a set threshold is reached, or once the user determines that enough data has been generated.

One concern arises from how much of the ``black box'' of our model input and training to show the user. While greater transparency into our procedure may alleviate doubt~\cite{sacha}, we must be careful not to introduce doubt by creating confusing, multiple levels of uncertainty (uncertainty in our model as well as uncertainty in the predicted uncertainty!). (TO DO: describe what we decided on for this.)

\section{Results}
For several choices made in the processing and modeling stages, the best option remained ambiguous. These ambiguities speak to the difficulty of the task, namely, applying data analytic methods to scientific data. One such choice was which sampling intervals or bins to use. Most important is for these choices to be domain-driven; scientists should be able to access data for scales that they are interested in. Another choice concerns whether we need to use physical units, or provide some scaling or indexing of these units by hand. The relationships that our model learns may seem arbitrary with respect to physical units, but these units are important for scientific understanding.

TO DO: Discuss whether we needed to scale explicitly, or whether the models handled it adequately.
     
To calculate the multi-scale model with all members of the five-figure summary as training inputs and outputs would require calculating a surface of at least eight dimensions. This required an impractical amount of computing power and memory, so we calculated each number separately, using only the input value for this number as training for its output, rather than using all five figures as input. This may have limited the predictive power of our model. (TO DO: maybe we can create some condensed summary of this five-number description.)

\subsection{Performance}
Preliminary results, for the cosmology dataset with the multi-scale model method, are shown in Figure~\ref{placeholder_results}. These results are very preliminary, because we haven't yet measured the variance of the ground truth dataset to much accuracy; we need more iterations. But the HMF itself is predicted very well, with almost no care given to model tuning, so this is promising.

For each test, we will optimize performance and then report and compare:
\begin{itemize}
\item relative error of model fit
\item relative closeness compared to ground truth
\item time to calculate model estimates and ground truth data
\item plots of predicted and true properties.
\end{itemize}

We will also report on the computing time required for data preprocessing, for each of the applications, including steps like halo finding.

\begin{figure*}
 \center
  \includegraphics[width=\textwidth]{images/placeholder_first_msm_hmf}
  \caption{PLACEHOLDER - preliminary results for one test. Blue represents predicted uncertainties; green represents ground truth. Because we need to generate much more data to properly measure the ground truth, the currently shown ground truth variance is underestimated. However, we can see that for lower mass bins, for which our ground truth data are better, our predictions are close.}
  \label{placeholder_results}
\end{figure*}

\subsection{Use Cases}
We will work with scientists to test our interface and develop test cases. The types of uses we will suggest include:

\begin{itemize}
\item Testing of effects of using different halo-finding methods (cosmology)
\item altering initial conditions and seeing how uncertainty changes (climate and cosmology)
\item how do parameterizations affect quality of/certainty in outputs? (climate)
\item altering halo definition, linking length, etc. (or other analysis parameters) (cosmology)
\item understanding in which parts of the domain the quantities are calculated more or less accurately (climate and cosmology)
\item ``how can i minimize uncertainty in the domain areas I care about?'' (climate and cosmology)
\item simply getting a much faster variance estimate than with conventional techniques (climate and cosmology)
\end{itemize}

\section{Discussion}

We will discuss these, among other topics:

\begin{itemize}
\item What are the limits of our method? i.e., what types of uncertainties, on which types of data, can be approximated with this bootstrapping process?
\item Can also talk more about various approaches to interpreting uncertainty, as in~\cite{knutti}.
\end{itemize}

\section{Conclusion}
Outline of what will hopefully be our conclusions:
\begin{itemize}
\item we have outlined a generalizable pipeline for setting up a discreteness-based uncertainty model for an arbitrary scientific dataset and operation
\item we have outlined a process for users trying our method with a new dataset to verify that this method will work for them, with little processing time lost.
\item we have contributed to the need for more confidence in visually-aided precision simulation analysis, with the goals of assessing galaxy formation, climate phenomena, etc.
\end{itemize}

Future work:
\begin{itemize}
\item adding capabilities to our system, such as sensitivity analysis
\item stringing together multiple analysis steps and modeling that uncertainty
\item can this type of analysis--evaluating relative importance of various uncertainty sources--be done in our visualization?
\end{itemize}

\section{Acknolwedgments}
Thanks to the High Energy Physics group at Argonne National Laboratory for providing data and ....

We also acknowledge the World Climate Research Programme's Working Group on Coupled Modelling, which is responsible for CMIP, and we thank the climate modeling groups (listed in Table XX of this paper) for producing and making available their model output. For CMIP the U.S. Department of Energy's Program for Climate Model Diagnosis and Intercomparison provides coordinating support and led development of software infrastructure in partnership with the Global Organization for Earth System Science Portals.

% \bibliographystyle{unsrt}
% \printbibliography
\bibliographystyle{abbrv}
\bibliography{references}
%%use following if all content of bibtex file should be shown
%\nocite{*}
% \bibliography{references}
\end{document}
