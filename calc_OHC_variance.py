#################################################
#                                               #
#  • given the OHC for a certain subset,        #
#  • calculate its uncertainty                  #
#  • using bootstrapping over the set of models #
#                                               #
#################################################

import numpy as np
np.set_printoptions(threshold=np.nan)
import os
import csv
import random
import matplotlib.pyplot as plt
from scipy.stats import scoreatpercentile

path = './data/' # path to CSVs (update once we actually have data)

def main():
    global frac
    frac = input('Fraction: ')
    start_year = input('Start year: ')
    global end_year
    end_year = input('End year (exclusive): ')
    global n_iterations
    n_iterations = int(input('Number of iterations: '))
    global n_models
    n_models = 5 # excluding the negatively-trending model...
    ohc_path = path + 'OHCs_' + start_year + '-' + end_year + '_' + frac + '.csv' # ...or whatever
    #fn = path + 'OHCs_all_2006-2009.csv' # bc i don't have consistent file names yet. one day
    vol_path = path + 'volumes_' + frac + '.csv'

    timesteps = np.arange('2006-01', str(end_year) + '-01', dtype='datetime64[M]')
    global n_timesteps
    n_timesteps = len(timesteps)
    OHCs = readfile(ohc_path)
    volumes = readfile(vol_path)[0] #since this is 1D
    full_volumes = readfile(path + 'volumes_1.0.csv')[0]
    
    OHCs = normalize(OHCs, volumes, full_volumes)
    indices = np.array([0,1,3,4,5])
    OHCs = OHCs[indices]
    OHC_change = calcChange(OHCs)
    for ohc in OHC_change:
        plt.plot(timesteps, ohc, linestyle='solid')
    plt.show()

    MMMs = np.empty([n_iterations, n_timesteps]) # multi-model means (the quantities we're finding the variance of)
    
    for i in range(n_iterations):
        bootstrap_OHCs = [random.choice(OHC_change) for _ in OHC_change]
        MMMs[i,:] = calcMMM(bootstrap_OHCs)

    for mmm in MMMs:
        plt.plot(timesteps, mmm, linestyle='solid')
    plt.show()
    
    calcVariance(MMMs)

def readfile(fn):
    OHCs = []
    with open(fn, 'r') as csvfile:
        ohcreader = csv.reader(csvfile, delimiter=',')
        for row in ohcreader:
            ohc_row = []
            for num in row:
                ohc_row.append(float(num))
            OHCs.append(ohc_row)
    return np.asarray(OHCs)

def normalize(data, vols, full_vols):
    normed_data = []
    for i in range(len(data)):
        normed_data.append(data[i]*full_vols[i]/vols[i])
    return np.asarray(normed_data)

def calcChange(data):
    change_data = []
    for i in range(len(data)): # for each model:
        row = []
        for j in range(len(data[i])):
            row.append(float(data[i][j] - data[i][0])) # compare against reference timestep
        change_data.append(row)
    return change_data

def calcMMM(OHCs):
    MMMs = np.empty([n_timesteps])
    for i in range(len(OHCs[0])):
        timestep_avg = 0
        for ohc in OHCs:
            timestep_avg += ohc[i]
        timestep_avg = timestep_avg/float(n_models)
        MMMs[i] = timestep_avg
    return MMMs # multi-model means, one per timestep, for this ensemble

def calcVariance(MMMs):
    timesteps = np.arange('2006-01', str(end_year) + '-01', dtype='datetime64[M]')
    means = []
    variances = []
    mins = []
    maxs = []
    meds = []
    lowers = []
    uppers = []

    for t in range(n_timesteps):
        mean = np.mean(MMMs[:,t])
        var = np.var(MMMs[:,t])
        min = np.min(MMMs[:,t])
        max = np.max(MMMs[:,t])
        med = np.median(MMMs[:,t])
        lower = scoreatpercentile(MMMs[:,t], 25)
        upper = scoreatpercentile(MMMs[:,t], 75)
    
        means.append(mean)
        variances.append(var)
        mins.append(min)
        maxs.append(max)
        meds.append(med)
        lowers.append(lower)
        uppers.append(upper)
    
    filename = path + 'CMIP_variance_N_' + str(n_iterations) + '_frac_' + frac + '_adjusted.csv'
    f = open(filename, "w")
    for i in range(0, n_timesteps):
        line = "%s\t%e\t%e\t%e\t%e\t%e\t%e\n" %(timesteps[i], variances[i], mins[i], maxs[i], meds[i], lowers[i], uppers[i])
        f.write(line)
    f.close()
    
    # plot MMM per timestep
    adjusted_mins = mins*np.full(len(mins), 1.0/(float(frac)*float(frac)))
    adjusted_maxs = maxs*np.full(len(mins), 1.0/(float(frac)*float(frac)))
    adjusted_meds = meds*np.full(len(mins), 1.0/(float(frac)*float(frac)))
    adjusted_lowers = lowers*np.full(len(mins), 1.0/(float(frac)*float(frac)))
    adjusted_uppers = uppers*np.full(len(mins), 1.0/(float(frac)*float(frac)))
    plt.plot(timesteps, adjusted_mins)
    plt.plot(timesteps, adjusted_maxs)
    plt.plot(timesteps, adjusted_meds)
    plt.plot(timesteps, adjusted_lowers)
    plt.plot(timesteps, adjusted_uppers)
    plt.show()
    
    # plots of convergence (one per timestep -- but they will all look very similar)
    """
    vars_per_it = np.empty([n_iterations, n_timesteps]) # for each timestep, a row of the variance at each iteration (cumulative)
    for it in range(1, n_iterations+1):
        vars_for_this_it = np.empty([n_timesteps])
        for t in range(n_timesteps): # calc variance per timestep
            var = np.mean(MMMs[0:it,t])
            vars_for_this_it[t] = var
            print("t: ", t, " var: ", var)
        vars_per_it[it-1] = vars_for_this_it
    for t in range(n_timesteps): # one plot per timestep
        its = np.arange(0, n_iterations)
        print(vars_per_it[:,t])
        plt.plot(its, vars_per_it[:,t], linestyle='solid')
        plt.show()
    """

main()
