import yt
from yt.analysis_modules.halo_analysis.api import HaloCatalog
from yt.analysis_modules.halo_mass_function.api import *

from astropy.stats import bootstrap
from astropy.utils import NumpyRNGContext

import numpy as np
import matplotlib.pyplot as plt

import h5py

path = "raw_data/"
halopath = "halo_catalogs/catalog/catalog.0.h5"

indices = []

def main():
  sub_ds = yt.load(randomSubset('snap_135.0.hdf5', 1.0))  #SET SUBSET SIZE HERE
  getHMF(sub_ds)
  
def getHMF(ds):
  hc = HaloCatalog(data_ds=ds, finder_method='fof') #SET HALO FINDER METHOD HERE (and corresponding parameters)
  hc.create()
  hc.save_catalog()
  halos_ds = yt.load('halo_catalogs/catalog/catalog.0.h5')
  hmf = HaloMassFcn(halos_ds=halos_ds)
  hmf.write_out(prefix='hmf', analytic=False, simulated=True) #specify HMF filename here
  plt.loglog(hmf.masses_sim, hmf.n_cumulative_sim)
  plt.show()

def randomSubset(filename, frac):
    ds = yt.load(path + filename)
    f = h5py.File(path + filename, 'r')
    
    ad = ds.all_data()
    arr = np.arange(int(ad['ParticleIDs'].size))
    datasize = ad['ParticleIDs'].size
    subsize = int(frac * datasize)
    
    #with NumpyRNGContext(1):
    #    bootarr = bootstrap(arr, 1, subsize)
    bootarr = np.random.choice(arr, subsize)
    print("returning random sample of size ", subsize)
    bootarr = bootarr.astype(int)

    #create new HDF5 file with only bootstrapped subset
    subFile = h5py.File('mySubset.0.hdf5', 'w')
    f.copy('Header', subFile)
    f.copy('PartType1', subFile)
    numparts = f['Header'].attrs['NumPart_ThisFile']
    numparts[1] = subsize

    subFile['Header'].attrs.modify('NumPart_ThisFile', numparts)
    subFile['Header'].attrs.modify('NumPart_Total', numparts)
    subFile['Header'].attrs.modify('NumFilesPerSnapshot', 1)

    # adjust the order of the axes -- not sure why they are out of order
    coordinates = ad['Coordinates'][bootarr]
    particleIDs = ad['ParticleIDs'][bootarr]
    potential = ad['Potential'][bootarr]
    velocities = ad['Velocities'][bootarr]

    del subFile['PartType1/Coordinates']
    del subFile['PartType1/ParticleIDs']
    del subFile['PartType1/Potential']
    del subFile['PartType1/Velocities']
    subFile['PartType1'].create_dataset('Coordinates', data=coordinates)
    subFile['PartType1'].create_dataset('ParticleIDs', data=particleIDs)
    subFile['PartType1'].create_dataset('Potential', data=potential)
    subFile['PartType1'].create_dataset('Velocities', data=velocities)
    
    return 'mySubset.0.hdf5'

main()
