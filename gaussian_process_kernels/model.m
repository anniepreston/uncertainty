close all;
clear variables;
clc;

% Load Data
path = '/Users/anniepreston/repos/hmfs/data/';

% Number and sizes of subsets (NOTE: using fixed values for now)
NSubsets = 3;
Link = '0.1';
sizes = [0.001 0.005 0.01];
starts = [130 130 130];
stops = [150 150 150];

NBins = 50;


% FIXME: combine all the loading below into a single function

% load the training data
for i = 1:NSubsets
    filePath = strcat(path, num2str(sizes(i)), '_', Link, '_', '/Variance_Average_/', num2str(sizes(i)), '_', Link, '_', num2str(starts(i)), '-', num2str(stops(i)), '.dat');
    file = fopen(filePath, 'r');
    new_cell = textscan(file, '%f\t%f\t%f\t%f\t%f\t%f\t%f\t');
    data = [data; new_cell];
    fclose(file);
end

% load the ground truth data
truth_data = cell(0);
filePath = strcat(path, '1.0', '_', Link, '/Variance_Average_1.0_', Link, '_', num2str(30), '-', num2str(48), '.dat');
file = fopen(filePath, 'r');
new_cell = textscan(file, '%f\t%f\t%f\t%f\t%f\t%f\t%f\t');
truth_data = [truth_data; new_cell];
fclose(file);

% create arrays of training data...
in_dim = 2; mass, fraction
out_dim = 5; % NOTE: if this is underdetermined (?) or just not doing well, we can reduce to 1 output dimension, performed 5x

N = NBins * (NSubsets+1); //number of features

inputData = zeros(N, in_dim);
outputData = zeros(N, out_dim);

% mass:
inputData(0:N,1) = logspace(1, 50, 50);  % NOTE: here is where we define the mass bins. change as needed...
% fractions:
for i = 1:NSubsets
    inputData((i-1)*NBins+1:i*NBins,2) = sizes(i);
end
inputData(N-NBins+1:N,2) = 1.0;

outputData(0:N-NBins,1) = data{1,3}(1:50);
outputData(0:N-NBins,2) = data{1,4}(1:50);
outputData(0:N-NBins,3) = data{1,5}(1:50);
outputData(0:N-NBins,4) = data{1,6}(1:50);
outputData(0:N-NBins,5) = data{1,7}(1:50);

% fill the rest of the output data with the ground truth (to be masked) :
outputData(N-NBins+1,1) = truth_data{

% mask the remaining data:
mask = ones(N,5);
mask(N-NBins+1:N,1:5) = 0;

% fill training input and output arrays
% N x P input vector, for N datapoints and P total input dimensions

xg = {inputData(0:N,1)',inputData(0:N,2)'};

x = covGrid('expand',xg);
N = size(x,1);

% label the f=1.0 instances as missing data
mask = double(mask);
mask(mask==false)=NaN;
tmp = isnan(mask);
tmp = tmp(:);
idx = find(~tmp); % training input locations
idxstar = find(tmp); % missing input locations

d = outputData(:); %FIXME: output data format could be wrong
y = d(idx); % training data

% standardize data here??

% specify a spectral mixture kernel in each input dimension, each with Q components.
Q = 20;
cov = {{'covSMfast',Q},{'covSMfast',Q}}; % 1D SM kernel for each input dimension, with Q components
covg = (@covGrid, cov, xg};

% mean function for the GP
gpmean = {@meanConst};
hyp.mean=mean(y); % if normalized, mean(y) will be zero

opt.cg_maxit = 200; opt.vg_tol = 1e-2;
if(result==2 || result==3}
    opt.cg_maxis = 400; 
end

inf_method = @(varargin) infGrid(varargin{:},opt);
lik = @likGauss;

% initialize the noise standard deviation (?)
sn = .1*mean(abs(y));
hyp.lik = log(sn);

% initialize spectral mixture kernel hyperparameters
inits = 100;
hyp = spectral_init(inf_method, hyp, gpmean, lik, cov, covg, x, y, idx, inits);

iters = 700;

% if you wish to use BFGS instead of non-linear conjugate gradients (see original code...)
%...

tic
hyp1 = minimize(hyp,@gp,-iters,inf_method,gpmean,covg,lik,idx,y);
toc

tic
[postg nlZg dnlZg] = infGrid(hyp1, gpmean, covg, 'likGauss', idx, y, opt);
toc

postg.L = @(x) 0*x;

% indices of points where we wish to make predictions:
% make predictions at all N input locations (training and testing)

star_ind = (1:N)';

tic
[ymug ys2g fmug fs2g] = gp(hyp1, @infGrid, gpmean, covg, [], idx, postg, star_ind);
toc
disp("GPatt results:");
disp(ymug);

% visualize something here:

disp('comparing to covSE extrapolation');

cov_SE = {{'covSEiso'},{'covSEiso'}}; % 1D SE kernel for each input dimension
covg_SE = {@covGrid, cov_SE, xg};

% initialize SE kernel hyperparameters

sn_SE = .1*mean(abs(y));
hypSE.lik = log(sn_SE);
ell = 20;
sf = std(y);
hypSE.cov = log([ell; sf; ell; sf]);

SE_iters = 1000;

% set mean function
gpmean_SE = {@meanConst};
hypSE.mean=mean(y);

tic
hypSE1 = minimize(hypSE, @gp, -SE_iters, inf_method, gpmean_SE, covg_SE, lik, idx, y);
toc

[postg_SE nlZgSE dnlZgSE] = infGrid(hypSE1, gpmean_SE, covg_SE, 'likGauss', idx, y, opt);

% indices of points where we wish to make predictions
% (for now: make predictions at all N input locations)
star_indx = (1:N)';

postg_SE.L = @(x) 0*x; % comment this line if you want predictive variances

[ymugSE ys2gSE fmugSE fs2gSE] = gp(hypSE1, infGrid, gpmean_SE, covg_SE, [], idx, postg_SE, star_ind);


