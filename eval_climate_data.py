import matplotlib.pyplot as plt
import numpy as np
import csv

def main():
    fracs = [0.0011461980934,0.000983801457207,0.00107869438466,0.00101038431624,0.000652484737947,0.00109275080901,0.0006354700792,0.000971319712345,0.0010059612831,0.00051229587871,0.00111129870955,0.00102376248913]

    fn = '/Users/anniepreston/repos/uncertainty/CMIP_data/OHCs_2006-2010_0.001.csv'
    data = []

    line = 0
    with open(fn, 'r') as f:
        reader = csv.reader(f, delimiter=',')
        for row in reader:
            data.append([float(num)/fracs[line] for num in row])
            line += 1

    boxplot_data = []
    for t in range(len(data[0])):
        timestep_data = []
        for m in range(len(data)):
            timestep_data.append(data[m][t]*10e-22)
        print(np.mean(np.asarray(timestep_data)))
        boxplot_data.append(timestep_data)

    x = np.arange(len(data[0]))
    for m in range(len(data)):
        plt.plot(x, data[m])

#    plt.boxplot(boxplot_data)
    plt.show()

main()